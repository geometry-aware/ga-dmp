clear all
close all
clc

% dbstop if warning
% dbstop if error

dbclear if warning
dbclear if error

addpath(genpath('factories'));
addpath(genpath('tools'));
myColors;

%% Load data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%load data/dataset_MAN/CShape_MAN.mat
load real_data/Stiff_prfile_water.mat
c_v = zeros(6,length(S));
for i=1:length(S)
    [c,p] = chol(S(:,:,i));
    mask = triu(true(size(c)),0);  
    c_v(:,i) = c(mask);
end
[a,b] = butter(4, .002);
c_v2 = (filtfilt(a,b,c_v'))';
figure; hold on;
plot(c_v(1,:)','color',mycolors.r); plot(c_v2(1,:)','--','color',mycolors.r);
plot(c_v(2,:)','color',mycolors.b); plot(c_v2(2,:)','--','color',mycolors.b);
plot(c_v(3,:)','color',mycolors.g); plot(c_v2(3,:)','--','color',mycolors.g);
plot(c_v(4,:)','color',mycolors.o); plot(c_v2(4,:)','--','color',mycolors.o);
plot(c_v(5,:)','color',mycolors.y); plot(c_v2(5,:)','--','color',mycolors.y);
plot(c_v(6,:)','color',mycolors.p); plot(c_v2(6,:)','--','color',mycolors.p);
for i=1:length(S)
    k = (triu(ones(3)));
    k(logical(k)) = c_v2(:,i);
    S(:,:,i) = k' * k;
end

Sorig = S;%GShape.cov;
%pos = msd(1).DataP;%GShape.demos{1}.pos;
[n_rows,n_cols,n] = size(Sorig);

%% Fit SPD data using piecewise geodesic interpolating
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
M = spdFactory(3);
tau = length(Sorig)/1000;
[Y, dy, ddy, t] = interpolationSPD(M, Sorig, tau);

%% Vectorieze SPD data for plotting purposes.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:length(Y)
    tmp = triu(Y{i,1});
    mask = triu(true(size(tmp)),0);
    Yorig(:,i) = tmp(mask);
    tmp = triu(S(:,:,i));
    mask = triu(true(size(tmp)),0);
    Yorig2(:,i) = tmp(mask);
end
clear S;
%% SET DMP parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dt=t(2)-t(1);

N = 70;
alpha_qGoal = 0.5;
alpha_z = 48/1;
alpha_x = 2;
alpha_pPos = 400;
alpha_pOri = 4;
DOF = n_rows * (n_rows + 1)/2;
y0 = Y{1,1};
yg = Y{end,1};

%% Initialize DMP object
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp(['---DMP for ' M.name() '---']);

DMP = riemanDMP(M, DOF, N, alpha_z,alpha_x,tau,dt,y0,yg);

%% DMP learn
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DMP = learn(DMP, Y, dy, ddy);

%% Initial states
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initial states
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
duration = 0;

S.y = DMP.y;
S.g = DMP.g;
S.gnew = DMP.g;
goalSind = 2*n;

% Goal switching
goal_switching = 0;
if goal_switching
    theta = 90;
    R = [cosd(theta) -sind(theta); sind(theta) cosd(theta)];
    newGoal = [9.2535    5.2643;    5.2643    9.7555];
    newGoal = R' * DMP.g * R;
    goalSind = 500;
end

XX = zeros(n_rows,n_cols,round((DMP.tau-duration)/DMP.dt)+1);
XX(:,:,1) = S.y;

Q = zeros(round((DMP.tau-duration)/DMP.dt)+1, M.dim());
tmp = triu(S.y);
Q(1,:) = tmp(triu(true(size(tmp)),0));

dQ = zeros(round((DMP.tau-duration)/DMP.dt)+1, M.dim());
S.z =  zeros(M.dim(),1);%dy(:,1); 
dQ(1,:) = S.z;

ddQ = zeros(round((DMP.tau-duration)/DMP.dt)+1, M.dim());
S.zd = zeros(M.dim(),1);%ddy(:,1);
ddQ(1,:) = S.zd;

x = zeros(round((DMP.tau-duration)/DMP.dt)+1, 1);
S.x = 1;
x(1) = 1;

time = zeros(round((DMP.tau-duration)/DMP.dt)+1, 1);

FX = zeros(round((DMP.tau-duration)/DMP.dt)+1, M.dim());
PSI = zeros(round((DMP.tau-duration)/DMP.dt)+1,N);

i = 2;
%% Run DMP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
while S.x > exp(-DMP.alpha_x * (DMP.tau - duration + DMP.dt) / DMP.tau)
    if goal_switching && i == goalSind
        S.gnew = newGoal;
    end
    S = integration(DMP, S);
    x(i) = S.x;
    XX(:,:,i) = S.y;
    dQ(i,:) = S.z'/DMP.tau;
    ddQ(i,:) = S.zd'/DMP.tau;
    FX(i,:) = S.f';
    PSI(i,:) = S.psi;
    time(i) = time(i-1) + DMP.dt;
    tmp = triu(S.y);
    Q(i,:) = tmp(triu(true(size(tmp)),0));


    i = i + 1;
end
spd_elem_fig = figure('position',[10 10 390 200]);hold on;box on;
p2 = plot(time, Q(:,1), 'color', mycolors.cw,'linewidth',2); %K_11
p3 = plot(time, Q(:,2), 'color', mycolors.cg,'linewidth',2); %K_22
p4 = plot(time, Q(:,3), 'color', mycolors.cb,'linewidth',2); %K_33
p5 = plot(time, Q(:,4), 'color', mycolors.cy,'linewidth',2); %K_12
p6 = plot(time, Q(:,5), 'color', mycolors.lg,'linewidth',2); %K_23
p7 = plot(time, Q(:,6), 'color', mycolors.cr,'linewidth',2); %K_13
p1 = plot(time, Yorig','k--','linewidth',2);

return
%% Plotting PSI
spd_psi_fig = figure('position',[10 10 390 200]);hold on;
plot(time,PSI,'linewidth',1.5)
for ll = 1:length(PSI)
    wPSI(ll,:) = PSI(ll,:)'.*DMP.w(1,:)';
end
axis tight
ylabel('$\\Psi_i(x)$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
print(spd_psi_fig,'-dsvg','-r600','results/spd_psi_fig');
print(spd_psi_fig,'-dpdf','-r600','results/spd_psi_fig');
print(spd_psi_fig,'-dpng','-r600','results/spd_psi_fig');
%% Plotting wPSI
spd_wpsi_fig = figure('position',[10 10 390 200]);hold on;
plot(time,wPSI,'linewidth',1.5)
axis tight
ylabel('$\w_i\Psi_i(x)$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
print(spd_wpsi_fig,'-dsvg','-r600','results/spd_wpsi_fig');
print(spd_wpsi_fig,'-dpdf','-r600','results/spd_wpsi_fig');
print(spd_wpsi_fig,'-dpng','-r600','results/spd_wpsi_fig');

%% Plotting SPD upertriangle elements
spd_elem_fig = figure('position',[10 10 390 200]);hold on;box on;
p1 = plot(time, Yorig','k--','linewidth',2);
p2 = plot(time, Q(:,1), 'color', mycolors.cw,'linewidth',1.5); %K_11
p3 = plot(time, Q(:,2), 'color', mycolors.cg,'linewidth',1.5); %K_22
p4 = plot(time, Q(:,3), 'color', mycolors.cb,'linewidth',1.5); %K_33
p5 = plot(time, Q(:,4), 'color', mycolors.cy,'linewidth',1.5); %K_12
p6 = plot(time, Q(:,5), 'color', mycolors.lg,'linewidth',1.5); %K_23
p7 = plot(time, Q(:,6), 'color', mycolors.cr,'linewidth',1.5); %K_13
legend([p1(3) p2 p3 p4 p5 p6 p7],'$C_{demo}$',...
    '$C_{11}$','$C_{22}$','$C_{33}$','$C_{12}$','$C_{23}$','$C_{13}$')
ylabel('SPD elements', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10); axis tight
print(spd_elem_fig,'-dsvg','-r600','results/spd_elem_fig');
print(spd_elem_fig,'-dpdf','-r600','results/spd_elem_fig');
print(spd_elem_fig,'-dpng','-r600','results/spd_elem_fig');

%% Plot distances
dist_logeuclid = zeros(round((DMP.tau-duration)/DMP.dt)+1, 1);
spd_dist_fig=figure('position',[10 10 390 200]);hold on
for i=1:n
    dist_logeuclid(i) = M.dist(XX(:,:,i),Y{i,1},'logeuclid');
end
plot(dist_logeuclid,'color',mycolors.b,'linewidth',1.5);%plot(d_KL);
ylabel('$distance$', 'Fontsize', 10);
xlabel('$Time$', 'Fontsize', 10);
set(gca,'Fontsize', 10);
print(spd_dist_fig,'-dsvg','-r600','results/spd_dist_fig');
print(spd_dist_fig,'-dpdf','-r600','results/spd_dist_fig');
print(spd_dist_fig,'-dpng','-r600','results/spd_dist_fig');


%% Plot stiffness over Cartesian
%pos = [GShape.demos{1}.pos(1,:); GShape.demos{1}.pos(2,:)];   
HP = [];
spd_cart_fig=figure('position',[10 10 390 300]);hold on; 
for i=round(linspace(1,n,n/2))
    [HP1, ~] = plot2x2elipsoids(pos(1:2,i), 1*Y{i,1}, [.7 .7 .7], .1,0); % Scaled matrix!
end
for i=round(linspace(1,n,10))
    [HP2, ~] = plot2x2elipsoids(pos(1:2,i), 1*XX(:,:,i), [0 1 0], .4); % Scaled matrix!
end
HP = [HP1(1) HP2(1)];
legText{1} = '$\bm{C}$';
legText{2} = '$\bm{\hat{C}}$';
axis equal;
if goal_switching
    [HP3, ~] = plot2x2elipsoids(pos(1:2,goalSind), 1*XX(:,:,goalSind),...
        [0 0 1], .4);
    legText{3} = 'Switching starts';
    [HP4, ~] = plot2x2elipsoids(pos(1:2,end), 1*S.gnew,...
        [1 0 0], .4);
    HP = [HP HP3(1) HP4(1)];
    legText{4} = 'New goal';
end
ylabel('$y$', 'Fontsize', 10);
xlabel('$x$', 'Fontsize', 10);
set(gca, 'Fontsize', 10);
print(spd_cart_fig,'-dsvg','-r600','results/spd_cart_leg_fig');
print(spd_cart_fig,'-dpdf','-r600','results/spd_cart_leg_fig');
print(spd_cart_fig,'-dpng','-r600','results/spd_cart_leg_fig');
h = legend(HP, legText, 'Location','northwest');
set(h, 'Fontsize', 10,'color','none');
clear HP HP1 HP2 HP3 HP4 legText

%% Plot stiffness over Time
spd_Time_fig=figure('position',[10 10 450 170]);hold on;axis off;
HP = []; 
for i=round(linspace(1,n,n/2))
    [HP1, ~] = plot2x2elipsoids([i/100;0], .01*Y{i,1}, [.7 .7 .7], .1,0); 
end
for i=round(linspace(1,n,10))
    [HP2, ~] = plot2x2elipsoids([i/100;0], .01*XX(:,:,i), [0 1 0], .4);
end
HP = [HP1(1) HP2(1)];
legText{1} = '$\bm{C}$';
legText{2} = '$\bm{\hat{C}}$';
if goal_switching
    [HP3, ~] = plot2x2elipsoids([goalSind/100;0], .01*XX(:,:,goalSind), [0 0 1], .4); 
    legText{3} = 'Switching starts';
    [HP4, ~] = plot2x2elipsoids([n/100;0], .01*S.gnew, [1 0 0], .4); 
    HP = [HP HP3(1) HP4(1)];
    legText{4} = 'New goal';
end
axis tight;
figure
h = legend(HP, legText, 'Location','northwest');
set(h, 'Fontsize', 10);
ylabel('$\bm{C} \& \bm{\hat{C}}$', 'Fontsize', 7);
xlabel('Time', 'Fontsize', 10);
set(gca, 'Fontsize', 9);
print(spd_Time_fig,'-dsvg','-r600','results/spd_Time_fig');
print(spd_Time_fig,'-dpdf','-r600','results/spd_Time_fig');
print(spd_Time_fig,'-dpng','-r600','results/spd_Time_fig');

%% Plot Velocity
spd_Vel_fig=figure('position',[10 10 450 170]);hold on
p1 = plot(time, dy','k--','linewidth',2);
p2 = plot(time, dQ(:,1), 'color', mycolors.cw,'linewidth',1.5); %K_11
p3 = plot(time, dQ(:,3), 'color', mycolors.cy,'linewidth',1.5); %K_12
p4 = plot(time, dQ(:,2), 'color', mycolors.cg,'linewidth',1.5); %K_22
h = legend([p1(3) p2 p3 p4],'$\dot{C}_{demo}$','$\dot{C}_{11}$','$\dot{C}_{12}$','$\dot{C}_{22}$');
set(h, 'Fontsize', 10, 'Location','northwest', 'color','none');
ylabel('1st-time-derivative', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
axis tight;
set(gca, 'Fontsize', 9);
print(spd_Vel_fig,'-dsvg','-r600','results/spd_Vel_fig');
print(spd_Vel_fig,'-dpdf','-r600','results/spd_Vel_fig');
print(spd_Vel_fig,'-dpng','-r600','results/spd_Vel_fig');

%% Plot acceleration
spd_acc_fig=figure('position',[10 10 450 170]);hold on
p1 = plot(time, ddy','k--','linewidth',2);
p2 = plot(time, ddQ(:,1), 'color', mycolors.cw,'linewidth',1.5); %K_11
p3 = plot(time, ddQ(:,3), 'color', mycolors.cy,'linewidth',1.5); %K_12
p4 = plot(time, ddQ(:,2), 'color', mycolors.cg,'linewidth',1.5); %K_22
h = legend([p1(3) p2 p3 p4],'$\ddot{C}_{demo}$','$\ddot{C}_{11}$','$\ddot{C}_{12}$','$\ddot{C}_{22}$');
set(h, 'Fontsize', 10, 'Location','northwest', 'color','none');
ylabel('2nd-time-derivative', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
axis([0, time(end), -30, 30]);
set(gca, 'Fontsize', 9);
print(spd_acc_fig,'-dsvg','-r600','results/spd_acc_fig');
print(spd_acc_fig,'-dpdf','-r600','results/spd_acc_fig');
print(spd_acc_fig,'-dpng','-r600','results/spd_acc_fig');

%% Plot SPD in cone
spd_cone_fig = figure;
plotSPDprofileInCone(M, Y, 5,'color','k','LineStyle',':','linewidth',...
    4, 'view', [91.0305  -63.8606]);
plotSPDprofileInCone(M, XX, 5,'color',mycolors.cw,'LineStyle','-',...
    'linewidth',2, 'view', [91.0305  -63.8606]);
legend('','','$\bm{C}_{demo}$','','','$\bm{C}_{dmp}$')
print(spd_cone_fig,'-dsvg','-r600','results/spd_cone_fig');
print(spd_cone_fig,'-dpdf','-r600','results/spd_cone_fig');
print(spd_cone_fig,'-dpng','-r600','results/spd_cone_fig');

