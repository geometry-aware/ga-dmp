clear all
close all
clc

% dbstop if warning
% dbstop if error

dbclear if warning
dbclear if error

addpath(genpath('factories'));
addpath(genpath('tools'));

%% Load data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rawData = load('data/msd_data_03.mat');
variables=fields(rawData);
%Sorig = rawData.(variables{1}).cov;
Sorig = rawData.(variables{1}).Kp;
tmp = rawData.(variables{1}).DataP;
pos = tmp(1:2,:)';
[n_rows,n_cols,n] = size(Sorig);
myColors;

%% Fit SPD data using piecewise geodesic interpolating
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
M = spdFactory(2);
tau = 1;
[Y, dy, ddy, t] = interpolationSPD(M, Sorig, tau);

%% Vectorieze SPD data for plotting purposes.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:length(Y)
    tmp = triu(Y{i,1});
    mask = triu(true(size(tmp)),0);
    Yorig(:,i) = tmp(mask);
end

%% SET DMP parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dt=t(2)-t(1);

N = 70;
alpha_qGoal = 1;
alpha_z = 1*48/1;
alpha_x = 2;
alpha_pPos = 400;
alpha_pOri = 4;
DOF = M.dim();%n_rows * (n_rows + 1)/2;
y0 = Y{1,1};
yg = Y{end,1};

%% Initialize DMP object
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp(['---DMP for ' M.name() '---']);

DMP = riemanDMP(M, DOF, N, alpha_z,alpha_x,tau,dt,y0,yg);

%% DMP learn
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DMP = learn(DMP, Y, dy, ddy);
DMPG = DMP;

%% Initial states
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initial states
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
duration = 0;

S.y = DMP.y;
S.g = DMP.g;
S.gnew = DMP.g;
newGoal = DMP.g;
goalSind = 2*n;

% Goal switching
goal_switching = 1;
if goal_switching
    theta = 270;
    R = [cosd(theta) -sind(theta); sind(theta) cosd(theta)];
    %newGoal = [9.2535    5.2643;    5.2643    9.7555];
    newGoal = [200 0; 0 200];R' * DMP.g * R;
    goalSind = 50;
end

XX = zeros(n_rows,n_cols,round((DMP.tau-duration)/DMP.dt)+1);
XX(:,:,1) = S.y;
XXG = XX;

Q = zeros(round((DMP.tau-duration)/DMP.dt)+1, M.dim());
tmp = triu(S.y);
Q(1,:) = tmp(triu(true(size(tmp)),0));
QG = Q;

dQ = zeros(round((DMP.tau-duration)/DMP.dt)+1, M.dim());
S.z =  zeros(3,1);%dy(:,1); 
dQ(1,:) = S.z;

ddQ = zeros(round((DMP.tau-duration)/DMP.dt)+1, M.dim());
S.zd = zeros(3,1);%ddy(:,1);
ddQ(1,:) = S.zd;

x = zeros(round((DMP.tau-duration)/DMP.dt)+1, 1);
S.x = 1;
x(1) = 1;
SG = S;

time = zeros(round((DMP.tau-duration)/DMP.dt)+1, 1);

FX = zeros(round((DMP.tau-duration)/DMP.dt)+1, M.dim());
PSI = zeros(round((DMP.tau-duration)/DMP.dt)+1,N);

%compute  error
eQdemo = zeros(round((DMP.tau-duration)/DMP.dt)+1, 1);
eQdemoG = zeros(round((DMP.tau-duration)/DMP.dt)+1, 1);
for i = 1:length(eQdemo)
  eQdemo(i) = M.dist(Y{i,1},Sorig(:,:,end))';
  eQdemoG(i) = M.dist(Y{i,1},newGoal)';
end
eQ = zeros(round((DMP.tau-duration)/DMP.dt)+1, 1);
eQG = zeros(round((DMP.tau-duration)/DMP.dt)+1, 1);
eQ(1) = M.dist(S.y, newGoal)';
eQG(1) = M.dist(SG.y, newGoal)';

i = 2;
%% Run DMP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
while S.x > exp(-DMP.alpha_x * (DMP.tau - duration + DMP.dt) / DMP.tau)
    if goal_switching && i == goalSind
        SG.gnew = newGoal;
    end
    S = integration(DMP, S);
    SG = integration(DMPG, SG);
    x(i) = S.x;
    XX(:,:,i) = S.y;
    XXG(:,:,i) = SG.y;
    dQ(i,:) = S.z'/DMP.tau;
    ddQ(i,:) = S.zd'/DMP.tau;
    FX(i,:) = S.f';
    PSI(i,:) = S.psi;
    time(i) = time(i-1) + DMP.dt;
    tmp = triu(S.y);
    Q(i,:) = symMat2Vec(S.y);%tmp(triu(true(size(tmp)),0));
    tmp = triu(SG.y);
    QG(i,:) = symMat2Vec(SG.y);%tmp(triu(true(size(tmp)),0));

    eQ(i) = M.dist(S.y, newGoal)';
    eQG(i) = M.dist(SG.y, newGoal)';

    i = i + 1;
end
eqplotERRGoal = figure('position',[2158 655 800 180]);hold on;box on
plot(time,eQG, 'color',mycolors.cr, 'LineWidth', 2);
plot(time,eQ, 'k--', 'LineWidth', 2);

dist_logeuclid = zeros(round((DMP.tau-duration)/DMP.dt)+1, 1);
spd_dist_fig=figure('position',[729 -609 241 119]);hold on; box on;
for i=1:length(XX)
    %dist_logeuclid(i) = M.dist(XX(:,:,i),Y{i,1},'logeuclid');
    %dist_logeuclid(i) = M.dist(XX(:,:,i),newGoal,'logeuclid');
    dist_logeuclid(i) = M.dist(XXG(:,:,i),newGoal,'logeuclid');
end
plot(dist_logeuclid,'color',mycolors.b,'linewidth',1.5);%plot(d_KL);

spd_elem_fig = figure('position',[2158 383 800 180]);hold on; box on;
p2 = plot(time, QG(:,1), 'color', mycolors.cw,'linewidth',1.5); %K_11
p3 = plot(time, QG(:,3), 'color', mycolors.cy,'linewidth',1.5); %K_12
p4 = plot(time, QG(:,2), 'color', mycolors.cg,'linewidth',1.5); %K_22
p1 = plot(time, Q,'k--','linewidth',2);

HP = [];
spd_cart_fig=figure('position',[758 -572 221 170]);hold on; box on;
for i=round(linspace(1,n,n/2))
    [HP1, ~] = plot2x2elipsoids(pos(i,:)', .00002*Y{i,1}, [.7 .7 .7], .4,0.1); % Scaled matrix!
end
for i=round(linspace(1,n,20))
    [HP2, ~] = plot2x2elipsoids(pos(i,:)', .00002*XXG(:,:,i), mycolors.g, .4); % Scaled matrix!
end
HP = [HP1(1) HP2(1)];
legText{1} = '$\bm{C}$';
legText{2} = '$\bm{\hat{C}}$';
axis equal;
if goal_switching
    [HP3, ~] = plot2x2elipsoids(pos(goalSind,:)', .00002*XXG(:,:,goalSind),...
        [0 0 1], .4);
    legText{3} = 'Switching starts';
    [HP4, ~] = plot2x2elipsoids(pos(end,:)', .00002*newGoal,...
        [1 0 0], .4);
    HP = [HP HP3(1) HP4(1)];
    legText{4} = 'New goal';
end
%plot(pos(1,:), pos(2,:),'linewidth',1.5,'color','k');

return
%% Plotting PSI
spd_psi_fig = figure('position',[729 -609 241 119]);hold on; box on;
plot(time,PSI,'linewidth',1.5)
for ll = 1:length(PSI)
    wPSI(ll,:) = PSI(ll,:)'.*DMP.w(1,:)';
end
axis tight;
ylabel('$\\Psi_i(x)$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
print(spd_psi_fig,'-dsvg','-r600','results/spd_psi_fig');
print(spd_psi_fig,'-dpdf','-r600','results/spd_psi_fig');
print(spd_psi_fig,'-dpng','-r600','results/spd_psi_fig');
%% Plotting wPSI
spd_wpsi_fig = figure('position',[729 -609 241 119]);hold on; box on;
plot(time,wPSI,'linewidth',1.5)
axis tight
ylabel('$\w_i\Psi_i(x)$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
print(spd_wpsi_fig,'-dsvg','-r600','results/spd_wpsi_fig');
print(spd_wpsi_fig,'-dpdf','-r600','results/spd_wpsi_fig');
print(spd_wpsi_fig,'-dpng','-r600','results/spd_wpsi_fig');

%% Plotting SPD upertriangle elements
spd_elem_fig = figure('position',[729 -609 241 119]);hold on; box on;
p2 = plot(time, Q(:,1), 'color', mycolors.cw,'linewidth',1.5); %K_11
p3 = plot(time, Q(:,3), 'color', mycolors.cy,'linewidth',1.5); %K_12
p4 = plot(time, Q(:,2), 'color', mycolors.cg,'linewidth',1.5); %K_22
p1 = plot(time, Yorig','k-.','linewidth',2);
h = legend([p1(3) p2],'$\bcalC^{demo}$','colored $\bcalC^{dmp}$', 'color','none');
%legend([p1(3) p2 p3 p4],'$C_{demo}$','$C_{11}$','$C_{12}$','$C_{22}$')
ylabel('$\bcalC$ elements','Fontsize',10)
xlabel('Time', 'Fontsize', 10);
print(spd_elem_fig,'-dsvg','-r600','results/spd_elem_fig');
print(spd_elem_fig,'-dpdf','-r600','results/spd_elem_fig');
print(spd_elem_fig,'-dpng','-r600','results/spd_elem_fig');

%% Plot distances
dist_logeuclid = zeros(round((DMP.tau-duration)/DMP.dt)+1, 1);
spd_dist_fig=figure('position',[729 -609 241 119]);hold on; box on;
for i=1:length(XX)
    dist_logeuclid(i) = M.dist(XX(:,:,i),Y{i,1},'logeuclid');
end
plot(dist_logeuclid,'color',mycolors.b,'linewidth',1.5);%plot(d_KL);
ylabel('$error$', 'Fontsize', 10);
xlabel('$Time$', 'Fontsize', 10);
set(gca,'Fontsize', 10);
print(spd_dist_fig,'-dsvg','-r600','results/spd_dist_fig');
print(spd_dist_fig,'-dpdf','-r600','results/spd_dist_fig');
print(spd_dist_fig,'-dpng','-r600','results/spd_dist_fig');


%% Plot stiffness over Cartesian
HP = [];
spd_cart_fig=figure('position',[758 -572 221 170]);hold on; box on;
plot(pos(:,1), pos(:,2),'linewidth',1.5,'color','k');
for i=round(linspace(1,n,n/2))
    [HP1, ~] = plot2x2elipsoids(pos(i,:)', .00002*Y{i,1}, [.7 .7 .7], .4,0.1); % Scaled matrix!
end
for i=round(linspace(1,n,20))
    [HP2, ~] = plot2x2elipsoids(pos(i,:)', .00002*XX(:,:,i), mycolors.g, .4); % Scaled matrix!
end
HP = [HP1(1) HP2(1)];
legText{1} = '$\bm{C}$';
legText{2} = '$\bm{\hat{C}}$';
axis equal;
if goal_switching
    [HP3, ~] = plot2x2elipsoids(pos(goalSind,:)', .00002*XX(:,:,goalSind),...
        [0 0 1], .4);
    legText{3} = 'Switching starts';
    [HP4, ~] = plot2x2elipsoids(pos(end,:)', .00002*S.gnew,...
        [1 0 0], .4);
    HP = [HP HP3(1) HP4(1)];
    legText{4} = 'New goal';
end
ylabel('$y$', 'Fontsize', 10);
xlabel('$x$', 'Fontsize', 10);
set(gca, 'Fontsize', 10);
print(spd_cart_fig,'-dsvg','-r600','results/spd_cart_leg_fig');
print(spd_cart_fig,'-dpdf','-r600','results/spd_cart_leg_fig');
print(spd_cart_fig,'-dpng','-r600','results/spd_cart_goal_fig');
h = legend(HP, legText, 'Location','bestoutside','color','none');
set(h, 'Fontsize', 10,'color','none');
clear HP HP1 HP2 HP3 HP4 legText

%% Plot stiffness over Time
spd_Time_fig=figure('position',[729 -609 241 119]);hold on;axis on;box on
HP = []; 
for i=round(linspace(1,n,n/2))
    [HP1, ~] = plot2x2elipsoids([i/100;0], .00001*Y{i,1}, [.7 .7 .7], .4,0.1); 
end
for i=round(linspace(1,n,20))
    [HP2, ~] = plot2x2elipsoids([i/100;0], .00001*XX(:,:,i), mycolors.g, .4);
end
axis equal;axis tight
HP = [HP1(1) HP2(1)];
legText{1} = '$\Upsilon^{demo}$';
legText{2} = '$\Upsilon^{dmp}$';
if goal_switching
    [HP3, ~] = plot2x2elipsoids([goalSind/100;0], .00001*XX(:,:,goalSind), [0 0 1], .4); 
    legText{3} = 'Switching starts';
    [HP4, ~] = plot2x2elipsoids([n/100;0], .00001*S.gnew, [1 0 0], .4); 
    HP = [HP HP3(1) HP4(1)];
    legText{4} = 'New goal';
end
axis tight;
figure
h = legend(HP, legText, 'Location','northwest');
set(h, 'Fontsize', 10);
ylabel('$\bcalC^{demo} \& \\bcalC^{dmp}$', 'Fontsize', 7);
xlabel('Time', 'Fontsize', 10);
set(gca, 'Fontsize', 9);
print(spd_Time_fig,'-dsvg','-r600','results/spd_Time_fig');
print(spd_Time_fig,'-dpdf','-r600','results/spd_Time_fig');
print(spd_Time_fig,'-dpng','-r600','results/spd_Time_goal_fig');

%% Plot Velocity
spd_Vel_fig=figure('position',[729 -609 241 119]);hold on; box on;
p2 = plot(time, dQ(:,1), 'color', mycolors.cw,'linewidth',2); %K_11
p3 = plot(time, dQ(:,3), 'color', mycolors.cy,'linewidth',2); %K_12
p4 = plot(time, dQ(:,2), 'color', mycolors.cg,'linewidth',2); %K_22
p1 = plot(time, dy','k-.','linewidth',2);
h = legend([p1(3) p2],'$\bcalCd^{demo}$','colored $\bcalCd^{dmp}$', 'color','none');
%h = legend([p1(3) p2 p3 p4],'$\bcalCd^{demo}$','$\dot{C}_{11}$','$\dot{C}_{12}$','$\dot{C}_{22}$');
set(h, 'Fontsize', 10, 'Location','northwest', 'color','none');
ylabel('$\bcalCd$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
axis tight;
set(gca, 'Fontsize', 9);
print(spd_Vel_fig,'-dsvg','-r600','results/spd_Vel_fig');
print(spd_Vel_fig,'-dpdf','-r600','results/spd_Vel_fig');
print(spd_Vel_fig,'-dpng','-r600','results/spd_Vel_fig');

%% Plot acceleration
spd_acc_fig=figure('position',[729 -609 241 119]);hold on; box on;
p1 = plot(time, ddy','k--','linewidth',2);
p2 = plot(time, ddQ(:,1), 'color', mycolors.cw,'linewidth',1.5); %K_11
p3 = plot(time, ddQ(:,3), 'color', mycolors.cy,'linewidth',1.5); %K_12
p4 = plot(time, ddQ(:,2), 'color', mycolors.cg,'linewidth',1.5); %K_22
h = legend([p1(3) p2 p3 p4],'$\ddot{C}_{demo}$','$\ddot{C}_{11}$','$\ddot{C}_{12}$','$\ddot{C}_{22}$');
set(h, 'Fontsize', 10, 'Location','northwest', 'color','none');
ylabel('2nd-time-derivative', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
axis([0, time(end), -30, 30]);
set(gca, 'Fontsize', 9);
print(spd_acc_fig,'-dsvg','-r600','results/spd_acc_fig');
print(spd_acc_fig,'-dpdf','-r600','results/spd_acc_fig');
print(spd_acc_fig,'-dpng','-r600','results/spd_acc_fig');

%% Plot SPD in cone
spd_cone_fig = figure; r = 130; view = [58.9999   -0.2606];
plotSPDprofileInCone(M, XX, r,'color',mycolors.g,'LineStyle','-',...
    'linewidth',3, 'view', view);
plotSPDprofileInCone(M, Y, r,'color','k','LineStyle','-.','linewidth',...
    3, 'view', view);
legend('','','$\bcalC^{demo}$','','','$\bcalC^{dmp}$')
print(spd_cone_fig,'-dsvg','-r600','results/spd_cone_fig');
print(spd_cone_fig,'-dpdf','-r600','results/spd_cone_fig');
print(spd_cone_fig,'-dpng','-r600','results/spd_cone_fig');

%%
%% Plot distances
stifDistGoal=figure('position',[10 5 390 200]);hold on
for i=1:50
    d_le1(i) = M.dist(XX(:,:,i),Y{i,1});
end
for i=50:n
    d_le2(i-50+1) = M.dist(XX(:,:,i),S.gnew);
end
yyaxis left;
plot(1:50,d_le1,'color',mycolors.b);
ylabel('$distance$', 'Fontsize', 10, 'Interpreter', 'Latex');
yyaxis right
plot(50:n,d_le2,'color',mycolors.r);%plot(d_KL);plot(d_ld);plot(d_ri);
plot(50,linspace(0,2.3,50),'.','color',mycolors.g,'markersize',5,'linewidth',1.5);
ylabel('$distance$', 'Fontsize', 10, 'Interpreter', 'Latex');
yyaxis left;
text(50,0.005,'$\leftarrow$ Seperator','interpreter','latex','FontName','Times','Fontsize',12)
h = legend('$d_1$','$d_2$');
set(h, 'Fontsize', 10, 'Interpreter', 'Latex');
xlabel('$t$', 'Fontsize', 10, 'Interpreter', 'Latex');
set(gca,'TickLabelInterpreter','latex', 'Fontsize', 10);


function V = symMat2Vec(S)
% Vectorization of a tensor of symmetric matrix
[D, ~, N] = size(S);

V = [];
for n = 1:N
	v = [];
	v = diag(S(:,:,n));
	for d = 1:D-1
	  v = [v; sqrt(2).*diag(S(:,:,n),d)]; % Mandel notation
	%   v = [v; diag(M,n)]; % Voigt notation
	end
	V = [V v];
end

end