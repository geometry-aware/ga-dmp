% Generate SPD trajectory of a given points.
% transport all projections to a common tangent space, (e.g. of the first
% point).

%function [X, dx1i, ddx1i, tt] = generate_spd_data3(X, tau, dt)
function [y, dS, dds, t] = interpolationUQ(M, S, tau)

nbData = length(S);
%n = nbData*tau;
t = linspace(0,tau,nbData);
dataCoords = linspace(0,tau,nbData);

pdims = 3;


colorss = lines(20);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INTERPGEO returns the value Y of the piecewise geodesic interpolating spline at time T.
% Original author:
%   Pierre-Yves Gousenbourger, Feb. 27, 2018

% we should compare the logs beforehand (this can be done offline)
logs = zeros(pdims,1,nbData-1);
for i = 1:nbData-1
    logs(:,i) = M.log(S(:,i),S(:,i+1));
end

% evaluation at t of the curve
y = cell(nbData,1);%zeros(n_rows,n_cols,nbData);
tSpan1 = zeros(1,nbData);

for i = 1:nbData
    tStart = tic;
    % detect the closest data points
    idx = find(dataCoords - t(i) >= 0);
    if isempty(find(dataCoords == t(i))) % data coord not at t(i)
        a = S(:,idx(1)-1);
        t1 = dataCoords(idx(1)-1);
        t2 = dataCoords(idx(1));
        tt = (t(i) - t1)./(t2 - t1);
        % geodesic
        y{i,1} = M.exp(a,logs(:,idx(1)-1),tt)';%;
    else
        y{i,1} = S(:,idx(1))';
    end
    tSpan1(i) = toc(tStart);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute Velocity

% output
dS     = zeros(pdims,nbData);
dds     = zeros(pdims,nbData);
tSpan2 = zeros(1,nbData);

Zi(:,:,1) = y{1,1};
for i = 1:nbData-1
    tStart   = tic;
    dt = abs(t(i+1)-t(i));
    dS(:,i) = M.log(y{i,1},y{i+1,1})/dt;
    tSpan2(i) = toc(tStart);
end
% evaluation of the end velocity
tStart   = tic;
dt = abs(t(end)-t(end-1));
dS(:,i+1) = M.log(y{end-1,1},y{end,1})/dt;
tSpan2(end) = toc(tStart);

% Calculate derivatives
for j = 1:3
    dds(j,:) = gradient(dS(j,:), t);
end

end