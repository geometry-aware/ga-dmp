function [R, omega, domega] = convert_data(q, t)
%-------------------------------------------------------------------------
% Calculates logarithm of orientation difference between quaternions
    N = numel(t);
    R = cell(N,1);
    RR = zeros(9,N);
    dR = zeros(9,N);
    omega = zeros(3,N);
    domega = zeros(3,N);

    for i = 1:N
        R{i,1} = q2R(q(i,:));
        RR(:,i) = reshape(R{i,1}, 9, 1);
    end

    for i = 1:9
        dR(i,:) = gradient(RR(i,:), t);
    end
    for i = 1:N
        dRi = reshape(dR(:,i),3,3);
        Omega = dRi*R{i,1}';
        omega(:,i) = [Omega(3,2); Omega(1,3); Omega(2,1)];
    end

    for j = 1:3
        domega(j,:) = gradient(omega(j,:), t);
    end

    omega(:,1) = [0; 0; 0];
    omega(:,N) = [0; 0; 0];
    domega(:,1) = [0; 0; 0];
    domega(:,N) = [0; 0; 0];
end

function R = q2R(q)
        %-------------------------------------------------------------------------
        % Return the 3x3 rotation matrix described by a quaternion

        a = q(1)^2; b = q(2)^2; c = q(3)^2; d = q(4)^2;
        e = 2*q(2)*q(3); f = 2*q(1)*q(4);
        g = 2*q(2)*q(4); h = 2*q(1)*q(3);
        i = 2*q(3)*q(4); j = 2*q(1)*q(2);

        R = [a+b-c-d e-f     g+h; ...
            e+f     a-b+c-d i-j; ...
            g-h     i+j     a-b-c+d];
    end