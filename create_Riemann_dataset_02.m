clear all
close all
clc

%% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nbData = 200; % Number of datapoints
nbSamples = 1; % Number of demonstrations
nbIter = 5; % Number of iteration for the Gauss Newton algorithm
nbIterEM = 5; % Number of iteration for the EM algorithm
addpath(genpath('tools'));

%% Create robots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Robots parameters
nbDOFs = 3; % Nb of degrees of freedom for teacher robot
% armLength = 4; % For I and L
armLength = 5; % For C
L1 = Link('d', 0, 'a', armLength, 'alpha', 0);
robotT = SerialLink(repmat(L1,nbDOFs,1)); % Robot teacher
q0T = [pi/4 0.0 -pi/9]; % Initial robot configuration

%% Generate covariance data from handwriting data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
LASA = 0;
PLOT = 0;
if LASA
    % S. M. Khansari-Zadeh and A. Billard, "Learning Stable Non-Linear
    % Dynamical Systems with Gaussian Mixture Models", IEEE Transaction on
    % Robotics, 2011.
    p_path = './data/LASA_hand_writing_dataset/DataSet/';
    names = {'Angle','BendedLine','CShape','GShape', 'heee','JShape',...
        'Khamesh','LShape','NShape','PShape','RShape','Saeghe','Sharpc',...
        'Sine','Snake','Spoon','Sshape','Trapezoid','Worm','WShape',...
        'Zshape','JShape_2', 'Leaf_1', 'Leaf_2', 'DoubleBendedLine',...
        'Line','Multi_Models_1','Multi_Models_2','Multi_Models_3',...
        'Multi_Models_4'};
    DT = 0.001;
    scale = 100;
    nbSamples0 = 7;
    nbStates = 5;
else
    % Calinon S. Gaussians on Riemannian manifolds: Applications for robot
    % learning and adaptive control. IEEE Robotics & Automation Magazine.
    % 2020 Apr 6;27(2):33-45.
    p_path = 'data/2Dletters/';
    names = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N',...
        'O','P','Q','R','S','T','U','V','W','X','Y','Z'};
    dt = 0.01;
    scale = 50;
    nbSamples0 = 10;
    nbStates = 5;
end
%% Start
rotm_ref = eye(3);
quat_ref = [1 0 0 0];

for l = 1:length(names)
    load([p_path names{l} '.mat']);
    %dt = DT;
    nbData = length(demos{1}.pos);
    Xout = zeros(2,2,nbData); % Matrix storing t,x1,x2 for all the demos
    xIn = [1:nbData] * dt;
    wOut=[];    Data=[];
    uIn=[]; uOut=[];
    for n=1:nbSamples0
        tmp.Data = spline(1:size(demos{n}.pos,2), demos{n}.pos, linspace(1,size(demos{n}.pos,2),nbData)); %Resampling
        Data = [Data [xIn; tmp.Data]];
        wOut = [tmp.Data; mean(tmp.Data)];
        if ~LASA
            % Obtain robot configurations for the current demo given initial robot pose q0
            Tout = transl([tmp.Data(1:2,:) ; zeros(1,nbData)]');
            %T = transl(wOut');
            % One way to check robotics toolbox version
            if isobject(robotT.fkine(q0T))  % 10.X
                maskPlanarRbt = [ 1 1 0 0 0 0 ];  % Mask matrix for a 3-DoFs robots for position (x,y)
                q = robotT.ikine(Tout, q0T', 'mask', maskPlanarRbt)';  % Based on an initial pose
            else  % 9.X
                maskPlanarRbt = [ 1 1 0 0 0 0 ];
                q = robotT.ikine(T, q0T', maskPlanarRbt)'; % Based on an initial pose
            end
            % Computing force/velocity manipulability ellipsoids, that will be later
            % used for encoding a GMM in the force/velocity manip. ellip. manifold
            for t = 1 : nbData
                auxJ = robotT.jacob0(q(:,t),'trans');
                Jac = auxJ(1:2,:);
                Xout(:,:,t) = Jac*Jac'; % Saving ME
            end
        end
        %% Generate artificial unit quaternion as output from handwriting data
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        quat = quat_exp(wOut/scale,quat_ref');
        rotm = rot_exp(rotm_ref,wOut/scale);

        %% Calculate derivatives
        for j = 1:2
            vel(j,:) = gradient(tmp.Data(j,:), xIn);
        end
        tsVel = [vel; gradient(wOut(3,:), xIn)];
        for j = 1:2
            acc(j,:) = gradient(vel(j,:), xIn);
        end


        %% Storing
        demos{n}.quat = quat;       % Unit quaternion
        demos{n}.rotm = rotm;       % Rotation matrix
        demos{n}.tsPos = wOut;      % New 3D position on tangent space
        demos{n}.tsVel = tsVel;     % New 3D direvative on tangent space
        demos{n}.pos = tmp.Data;    % New 2D position
        demos{n}.vel = vel;         % New 2D velocity
        demos{n}.acc = acc;         % New 2D acceleration
        demos{n}.dt = dt;           % sample time
        if ~LASA
            demos{n}.joints = q;    % Joints
            demos{n}.manip = Xout;  % Manipulability
        end
    end

    %% Generate covariance data from handwriting data
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    mtmp.nbStates = nbStates;
    mtmp.nbVar = 3; % Dimension of the manifold and tangent space (1D input + 2^2 output)
    mtmp.nbVarCovOut = mtmp.nbVar + mtmp.nbVar*(mtmp.nbVar-1)/2; %Dimension of the output covariance
    mtmp.dt = dt; % Time step duration
    mtmp.params_diagRegFact = 1E-4;
    mtmp = init_GMM_kbins(Data, mtmp, nbSamples0);
    %mtmp = EM_GMM(Data, mtmp);
    [posOut, covOut] = GMR(mtmp, xIn, 1, 2:mtmp.nbVar);


    if PLOT
        spd_cart_fig=figure('position',[10 10 390 300]);hold on;
        for i=round(linspace(1,nbData,nbData/5))
            [HP1, ~] = plot2x2elipsoids(posOut(1:2,i), covOut(:,:,i), [.7 .7 .7], .5); % Scaled matrix!
        end
        spd_Time_fig=figure('position',[10 10 450 170]);hold on;axis off;
        for i=round(linspace(1,nbData,nbData/2))
            [HP1, ~] = plot2x2elipsoids([i/100;0], .00005*Xout(:,:,i), [.7 .7 .7], .5);
        end
        fig = figure;hold on;%figure('position',[280 85 589 441],'color',[1 1 1]);
        axis equal
        grid off
        axis off
        [kM,jM,iM] = sphere(50);
        h = surf(kM,jM,iM);
        set(h,'FaceAlpha',0.2)
        set(h,'EdgeColor','none')
        shading interp
        colormap(fig,'gray');
        axis equal
        view([160,25]);
        Q = demos{1}.quat';
        q = quaternion(Q);
        thetaPhi(:,1) = linspace(0+0.005,pi-0.005,1000);
        thetaPhi(:,2) = linspace(0+0.005,2*pi-0.005,1000);
        theta0 = thetaPhi(500,1);
        phi0   = thetaPhi(1,2);
        r0 = [cos(theta0);sin(theta0)*sin(phi0);sin(theta0)*cos(phi0)]';
        r0 = [0 0 1];
        pts = rotatepoint(q,r0);
        plot3(pts(:,1),pts(:,2),pts(:,3),'LineStyle','-','color','k','LineWidth',2)

        %% Plot SPD cone
        % prepare ax for plotting
        fig = figure;  set(gcf, 'color', [1 1 1]);  axis equal
        grid off
        hold on;
        r = 20;
        phi = 0:0.1:2*pi+0.1;
        x = [zeros(size(phi)); r.*ones(size(phi))];
        y = [zeros(size(phi));r.*sin(phi)];
        z = [zeros(size(phi));r/sqrt(2).*cos(phi)];

        h = mesh(x,y,z,'linestyle','none','facecolor',[.85 .85 .85],'facealpha',.7);
        direction = cross([1 0 0],[1/sqrt(2),1/sqrt(2),0]);
        rotate(h,direction,45,[0,0,0])

        h = plot3(x(2,:),y(2,:),z(2,:),'linewidth',1,'color',[.8 .8 .8]);
        rotate(h,direction,45,[0,0,0])
        axis off
        view(70,12);

        plot3(reshape(covOut(1,1,:),1,nbData), reshape(covOut(2,2,:),1,nbData), reshape(covOut(2,1,:),1,nbData), '.','markersize',12,'color',[.5 .5 .5]);
        
        %% Plot robot for manipulability
        figure('position',[10 10 1000 450],'color',[1 1 1]);hold on;
        p = [];
        for i = round(linspace(1,nbData,20))
            colTmp = [1,1,1] - [.8,.8,.8] * ((nbData-i+1)/nbData + 0.1);%(it+10)/nbIter;
            p = [p; plotArm(demos{1}.q(:,i), ones(nbDOFs,1)*armLength, [0; 0; i*0.1], .2, colTmp)];
        end
        plot(demos{1}.pos(1,:),demos{1}.pos(2,:),'r')
    end
    %% Save new data
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    eval([names{l},'.cov = covOut']);
    eval([names{l},'.posOut = posOut']);
    eval([names{l},'.demos = demos'])
    save(['./data/dataset_MAN/' names{l} '_MAN.mat'],names{l})
    clear mtmp model
end
return


%% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function y = quat_exp(w, q)
if nargin == 1
    q = [1 0 0 0]';
end
for i = 1:size(w,2)
    nrm_w = norm(w(:,i), 'fro');
    if nrm_w > 0
        y(:,i) = [cos(nrm_w/2); w(:,i)*sin(nrm_w/2)/nrm_w];
        y(:,i) = y(:,i)/norm(y(:,i));
        y(:,i) = quat_multiply(y(:,i),q);
    else
        y(:,i) = [1 0 0 0];
    end
end
end
function log_q = quat_log(q1,q2)
n = size(q2,2);
log_q = zeros(3,n);
for i = 1:n
    q2c = quat_conjugate(q2(:,i));
    q = quat_multiply(q1, q2c);

    tmp = norm(q);
    q = q/tmp;

    if norm(q(2:4)) > 1.0e-12
        log_q(:,i) = 2* acos(q(1)) * q(2:4)' / norm(q(2:4));
    else
        log_q(:,i) = [0; 0; 0];
    end
end
end

function q = quat_conjugate(q)
q(2:4) = -q(2:4);
end
function q = quat_multiply(q1, q2)
if isrow(q1),   q1 = q1';   end
if isrow(q2),   q2 = q2';   end
q(1) = q1(1) * q2(1) - q1(2:4)' * q2(2:4);
q(2:4) = q1(1) * q2(2:4) + q2(1) * q1(2:4) + ...
    [q1(3)*q2(4) - q1(4)*q2(3); ...
    q1(4)*q2(2) - q1(2)*q2(4); ...
    q1(2)*q2(3) - q1(3)*q2(2)];
end

function X = spd_expmap(U,S)
% Exponential map (SPD manifold)
N = size(U,3);
for n = 1:N
	X(:,:,n) = S^.5 * expm(S^-.5 * U(:,:,n) * S^-.5) * S^.5;
end
end

function U = spd_logmap(X,S)
% Logarithm map 
N = size(X,3);
for n = 1:N
% 	U(:,:,n) = S^.5 * logm(S^-.5 * X(:,:,n) * S^-.5) * S^.5;
% 	U(:,:,n) = S * logm(S\X(:,:,n));
	[v,d] = eig(S\X(:,:,n));
	U(:,:,n) = S * v*diag(log(diag(d)))*v^-1;
end
end

function RR = rot_exp(R,w, t)
if nargin < 3
    t = 1;
end
for i = 1:size(w,2)
    dR = vecMat(t*w(:,i));
    RR(:,:,i) = dR * R;
end
end
function R = vecMat(omega)
%-------------------------------------------------------------------------
% Return the rotation matrix equal to exp(omega)

    rn = norm (omega);
    if rn > 1.0e-12
        r = omega/rn;
        s = sin(rn); c = 1-cos(rn);

        srx = s*r(1); sry = s*r(2); srz = s*r(3);

        crx = c*r(1); cry = c*r(2); crz = c*r(3);

        crxrx = crx*r(1); cryry = cry*r(2); crzrz = crz*r(3);
        crxry = crx*r(2); cryrz = cry*r(3); crxrz = crx*r(3);

        R = [1-cryry-crzrz -srz+crxry sry+crxrz;...
            srz+crxry 1-crxrx-crzrz -srx+cryrz;...
            -sry+crxrz srx+cryrz 1-crxrx-cryry];
    else
        R = eye(3);
    end
end

function model = init_GMM_kbins(Data, model, nbSamples)
% Initialization of Gaussian Mixture Model (GMM) parameters by clustering 
% an ordered dataset into equal bins.
%
% Writing code takes time. Polishing it and making it available to others takes longer! 
% If some parts of the code were useful for your research of for a better understanding 
% of the algorithms, please reward the authors by citing the related publications, 
% and consider making your own research available in this way.
%
% @article{Calinon16JIST,
%   author="Calinon, S.",
%   title="A Tutorial on Task-Parameterized Movement Learning and Retrieval",
%   journal="Intelligent Service Robotics",
%   publisher="Springer Berlin Heidelberg",
%   doi="10.1007/s11370-015-0187-9",
%   year="2016",
%   volume="9",
%   number="1",
%   pages="1--29"
% }
% 
% Copyright (c) 2015 Idiap Research Institute, http://idiap.ch/
% Written by Sylvain Calinon, http://calinon.ch/
% 
% This file is part of PbDlib, http://www.idiap.ch/software/pbdlib/
% 
% PbDlib is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License version 3 as
% published by the Free Software Foundation.
% 
% PbDlib is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with PbDlib. If not, see <http://www.gnu.org/licenses/>.


%Parameters 
nbData = size(Data,2) / nbSamples;
if ~isfield(model,'params_diagRegFact')
	model.params_diagRegFact = 1E-4; %Optional regularization term to avoid numerical instability
end

%Delimit the cluster bins for the first demonstration
tSep = round(linspace(0, nbData, model.nbStates+1));

%Compute statistics for each bin
for i=1:model.nbStates
	id=[];
	for n=1:nbSamples
		id = [id (n-1)*nbData+[tSep(i)+1:tSep(i+1)]];
	end
	model.Priors(i) = length(id);
	model.Mu(:,i) = mean(Data(:,id),2);
	model.Sigma(:,:,i) = cov(Data(:,id)') + eye(size(Data,1)) * model.params_diagRegFact;
end
model.Priors = model.Priors / sum(model.Priors);

end

function [expData, expSigma, H] = GMR(model, DataIn, in, out)
% Gaussian mixture regression (GMR)
%
% If this code is useful for your research, please cite the related publication:
%
% @incollection{Calinon19MM,
%   author="Calinon, S.",
%   title="Mixture Models for the Analysis, Edition, and Synthesis of Continuous Time Series",
%   booktitle="Mixture Models and Applications",
%   publisher="Springer",
%   editor="Bouguila, N. and Fan, W.", 
%   year="2019"
% }
%
% Copyright (c) 2019 Idiap Research Institute, http://idiap.ch/
% Written by Sylvain Calinon, http://calinon.ch/
% 
% This file is part of PbDlib, http://www.idiap.ch/software/pbdlib/
% 
% PbDlib is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License version 3 as
% published by the Free Software Foundation.
% 
% PbDlib is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with PbDlib. If not, see <http://www.gnu.org/licenses/>.


nbData = size(DataIn,2);
nbVarOut = length(out);

if ~isfield(model,'params_diagRegFact')
	model.params_diagRegFact = 1E-8; %Regularization term is optional
end

MuTmp = zeros(nbVarOut, model.nbStates);
expData = zeros(nbVarOut, nbData);
expSigma = zeros(nbVarOut, nbVarOut, nbData);
for t=1:nbData
	%Compute activation weight
	for i=1:model.nbStates
		H(i,t) = model.Priors(i) .* gaussPDF(DataIn(:,t), model.Mu(in,i), model.Sigma(in,in,i));
	end
	H(:,t) = H(:,t) ./ sum(H(:,t)+realmin);
	%Compute conditional means
	for i=1:model.nbStates
		MuTmp(:,i) = model.Mu(out,i) + model.Sigma(out,in,i) / model.Sigma(in,in,i) * (DataIn(:,t)-model.Mu(in,i));
		expData(:,t) = expData(:,t) + H(i,t) .* MuTmp(:,i);
	end
	%Compute conditional covariances
	for i=1:model.nbStates
		SigmaTmp = model.Sigma(out,out,i) - model.Sigma(out,in,i) / model.Sigma(in,in,i) * model.Sigma(in,out,i);
		expSigma(:,:,t) = expSigma(:,:,t) + H(i,t) .* (SigmaTmp + MuTmp(:,i) * MuTmp(:,i)');
	end
	expSigma(:,:,t) = expSigma(:,:,t) - expData(:,t) * expData(:,t)' + eye(nbVarOut) * model.params_diagRegFact; 
end
end

function prob = gaussPDF(Data, Mu, Sigma)
% Likelihood of datapoint(s) to be generated by a Gaussian parameterized by center and covariance.
% Inputs -----------------------------------------------------------------
%   o Data:  D x N array representing N datapoints of D dimensions.
%   o Mu:    D x 1 vector representing the center of the Gaussian.
%   o Sigma: D x D array representing the covariance matrix of the Gaussian.
% Output -----------------------------------------------------------------
%   o prob:  1 x N vector representing the likelihood of the N datapoints.
%
% Writing code takes time. Polishing it and making it available to others takes longer! 
% If some parts of the code were useful for your research of for a better understanding 
% of the algorithms, please reward the authors by citing the related publications, 
% and consider making your own research available in this way.
%
% @article{Calinon16JIST,
%   author="Calinon, S.",
%   title="A Tutorial on Task-Parameterized Movement Learning and Retrieval",
%   journal="Intelligent Service Robotics",
%   publisher="Springer Berlin Heidelberg",
%   doi="10.1007/s11370-015-0187-9",
%   year="2016",
%   volume="9",
%   number="1",
%   pages="1--29"
% }
%
% Copyright (c) 2015 Idiap Research Institute, http://idiap.ch/
% Written by Sylvain Calinon, http://calinon.ch/
% 
% This file is part of PbDlib, http://www.idiap.ch/software/pbdlib/
% 
% PbDlib is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License version 3 as
% published by the Free Software Foundation.
% 
% PbDlib is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with PbDlib. If not, see <http://www.gnu.org/licenses/>.


[nbVar,nbData] = size(Data);
Data = Data - repmat(Mu,1,nbData);
prob = sum((Sigma\Data).*Data,1);
prob = exp(-0.5*prob) / sqrt((2*pi)^nbVar * abs(det(Sigma)) + realmin);

end