function M = so3Factory(n)

    M.name = @() sprintf('Rotations manifold SO(%d)', n);
    
    M.dim = @() nchoosek(n, 2);
    M.id = @() 'SO';
    
    M.inner = @(d1, d2) d1(:).'*d2(:);
    
    M.norm = @(d) norm(d(:));

    M.expm = @exponentialm;
    function Y = exponentialm(X, w, t)
        if nargin == 3
            exptU = t*w;
        else
            exptU = w;
        end
        exptU = expm(exptU);
        Y = X * exptU;
    end

    M.exp = @exponential;
    function RR = exponential(R,w, t)
        if nargin < 3
            t = 1;
        end
        dR = vecMat(t*w);
        RR = dR * R;
    end
    
    M.logm = @logarithmm;
    function U = logarithmm(X, Y)
		U = X'* Y;
            % The result of logm should be real in theory, but it is
            % numerically useful to force it.
            U = real(logm(U));
        % Ensure the tangent vector is in the Lie algebra.
        U = (U-U')/2;
    end

    M.log = @logarithm;
    function w = logarithm(R,Rg)
		do = vrrotmat2vec(Rg*R');
        w = do(4)*do(1:3)';
    end

    M.rand = @randrotion;
    function R = randrotion(n,k)
        if nargin < 1
            n = 3;
            k = 1;
        elseif nargin == 1
            k = 1;
        end
        R = randrot(n, k);
    end
    
    M.transp = @(x1, x2, d) d;
    M.isotransp = M.transp; % the transport is isometric
    
    M.pairmean = @pairmean;
    function Y = pairmean(X1, X2)
        V = M.log(X1, X2);
        Y = M.exp(X1, .5*V);
    end
    
    M.norm = @(d) norm(d(:));
    M.dist = @(x, y) M.norm(M.log(x, y));
    M.distm = @(x, y) M.norm(M.logm(x, y));
    
end

function R = vecMat(omega)
%-------------------------------------------------------------------------
% Return the rotation matrix equal to exp(omega)

    rn = norm (omega);
    if rn > 1.0e-12
        r = omega/rn;
        s = sin(rn); c = 1-cos(rn);

        srx = s*r(1); sry = s*r(2); srz = s*r(3);

        crx = c*r(1); cry = c*r(2); crz = c*r(3);

        crxrx = crx*r(1); cryry = cry*r(2); crzrz = crz*r(3);
        crxry = crx*r(2); cryrz = cry*r(3); crxrz = crx*r(3);

        R = [1-cryry-crzrz -srz+crxry sry+crxrz;...
            srz+crxry 1-crxrx-crzrz -srx+cryrz;...
            -sry+crxrz srx+cryrz 1-crxrx-cryry];
    else
        R = eye(3);
    end
end

function R = randrot(n, N)
% Generates uniformly random rotation matrices.
%
% function R = randrot(n, N)
%
% R is a n-by-n-by-N matrix such that each slice R(:, :, i) is an
% orthogonal matrix of size n of determinant +1 (i.e., a matrix in SO(n)).
% By default, N = 1.
% Complexity: N times O(n^3).
% Theory in Diaconis and Shahshahani 1987 for the uniformity on O(n);
% With details in Mezzadri 2007,
% "How to generate random matrices from the classical compact groups."
% To ensure matrices in SO(n), we permute the two first columns when
% the determinant is -1.
%
% See also: randskew

% This file is part of Manopt: www.manopt.org.
% Original author: Nicolas Boumal, Sept. 25, 2012.
% Contributors: 
% Change log: 

    if nargin < 2
        N = 1;
    end
    
    if n == 1
        R = ones(1, 1, N);
        return;
    end
    
    R = zeros(n, n, N);
    
    for i = 1 : N
        
        % Generated as such, Q is uniformly distributed over O(n), the set
        % of orthogonal matrices.
        A = randn(n);
        [Q, RR] = qr(A);
        Q = Q * diag(sign(diag(RR))); %% Mezzadri 2007
        
        % If Q is in O(n) but not in SO(n), we permute the two first
        % columns of Q such that det(new Q) = -det(Q), hence the new Q will
        % be in SO(n), uniformly distributed.
        if det(Q) < 0
            Q(:, [1 2]) = Q(:, [2 1]);
        end
        
        R(:, :, i) = Q;
        
    end

end
