function M = seFactory(n)
% Returns a manifold structure to optimize over the special Euclidean group
    
    elements = struct();
    elements.R = so3Factory(n);
    elements.t = vectorFactory(n);
    
    M = productmanifold(elements);

end
