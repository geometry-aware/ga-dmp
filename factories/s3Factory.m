function M = s3Factory()

    M.name = @() sprintf('Unit Quaternion S^3');
    M.dim = @() 3;
    M.id = @() 'S3';
    
    M.inner = @(d1, d2) d1(:).'*d2(:);
    
    M.norm = @(d) norm(d(:));

    M.normalize = @(d) d/norm(d);

    M.dist = @dist;
    function d = dist(x, y)
        
        % The following code is mathematically equivalent to the
        % computation d = acos(x(:)'*y(:)) but is much more accurate when
        % x and y are close.
        
        chordal_distance = norm(x - y, 'fro');
        d = real(2*asin(.5*chordal_distance));
        
        % Note: for x and y almost antipodal, the accuracy is good but not
        % as good as possible. One way to improve it is by using the
        % following branching:
        % % if chordal_distance > 1.9
        % %     d = pi - dist(x, -y);
        % % end
        % It is rarely necessary to compute the distance between
        % almost-antipodal points with full accuracy in Manopt, hence we
        % favor a simpler code.
        
    end



    M.expm = @exponentialm;
    function y = exponentialm(x, w, t)
        if nargin == 2
            % t = 1
            td = w;
        else
            td = t*w;
        end

        nrm_td = norm(td, 'fro');

        % Former versions of Manopt avoided the computation of sin(a)/a for
        % small a, but further investigations suggest this computation is
        % well-behaved numerically.
        if nrm_td > 0
            y = x*cos(nrm_td) + td*(sin(nrm_td)/nrm_td);
        else
            y = x;
        end
    end

    M.exp = @exponential;
    function y = exponential(q,w, t)
        if nargin == 2
            t = 1;
        end

        if iscolumn(q),   q = q';   end
        if iscolumn(w),   w = w';   end
        nrm_w = norm(w, 'fro');

        % Former versions of Manopt avoided the computation of sin(a)/a for
        % small a, but further investigations suggest this computation is
        % well-behaved numerically.
        if nrm_w > 0
            q0 = [cos(nrm_w*t/2), w*(sin(nrm_w*t/2)/nrm_w)];
            y = M.mult(q0,q);
            %y = [cos(nrm_w), td*(sin(nrm_w)/nrm_w)];
        else
            y = reshape(q, 1, []); %[1 0 0 0];
        end
    end
    
    M.mult = @multiply;
    function q = multiply(q1, q2)
        if isrow(q1),   q1 = q1';   end
        if isrow(q2),   q2 = q2';   end
        q(1) = q1(1) * q2(1) - q1(2:4)' * q2(2:4);
        q(2:4) = q1(1) * q2(2:4) + q2(1) * q1(2:4) + ...
            [q1(3)*q2(4) - q1(4)*q2(3); ...
             q1(4)*q2(2) - q1(2)*q2(4); ...
             q1(2)*q2(3) - q1(3)*q2(2)];

        %q(1) = q1(1) * q2(1) - dot(q1(2:4),q2(2:4));
        %q(2:4) = cross(q1(2:4),q2(2:4)) + ...
        %    q1(1) * q2(2:4) + q2(1) * q1(2:4);
    end
    M.conj = @conjugate;
    function q = conjugate(q)
        q(2:4) = -q(2:4);
    end

    M.logm = @logarithmm;
    function v = logarithmm(x1, x2)
        v = M.proj(x1, x2 - x1);
        di = M.dist(x1, x2);
        % If the two points are "far apart", correct the norm.
        if di > 1e-6
            nv = norm(v, 'fro');
            v = v * (di / nv);
        end
    end
    M.log = @logarithm;
    function log_q = logarithm(q2,q1)
        q2c = M.conj(q2);
        q = M.mult(q1, q2c);

        tmp = M.norm(q);
        q = q/tmp;

        if norm(q(2:4)) > 1.0e-12
            log_q = 2* acos(q(1)) * q(2:4)' / norm(q(2:4));
        else
            log_q = [0; 0; 0];
        end
    end

    M.rand = @randrotion;
    function R = randrotion(n,k)
        if nargin < 1
            n = 3;
            k = 1;
        elseif nargin == 1
            k = 1;
        end
        R = randrot(n, k);
    end
    
    M.transp = @(x1, x2, d) d;
    M.isotransp = M.transp; % the transport is isometric
    
    M.pairmean = @pairmean;
    function Y = pairmean(X1, X2)
        V = M.log(X1, X2);
        Y = M.exp(X1, .5*V);
    end
    
    M.norm = @(d) norm(d(:));
    M.dist = @(x, y) M.norm(M.log(x, y));
    M.distm = @(x, y) M.norm(M.logm(x, y));
    
end



