function M = vectorFactory(m)
% Returns a manifold struct to optimize over real matrices.
%
% function M = euclideanfactory(m)
% function M = euclideanfactory(m, n)
% function M = euclideanfactory([n1, n2, ...])
%
% Returns M, a structure describing the Euclidean space of real matrices,
% equipped with the standard Frobenius distance and associated trace inner
% product, as a manifold for Manopt.
%
% m and n in general can be vectors to handle multidimensional arrays.
% If either of m or n is a vector, they are concatenated as [m, n].
%

    
    dimensions_vec = [m, 1]; % We have a row vector.
    
    M.size = @() dimensions_vec;
    M.dim = @() m;
    M.id = @() 'R';
    
    M.name = @() sprintf('Euclidean vector space %s x 1', num2str(m));
    
    M.inner = @(d1, d2) d1.'*d2;
    
    M.norm = @(d) norm(d, 'fro');
    
    M.dist = @(x, y) norm(x - y, 'fro');
    
    M.exp = @exp;
    function y = exp(x, d, t)
        if iscolumn(d)
            d = d';
        end
        if nargin == 3
            y = x + t*d;
        else
            y = x + d;
        end
    end

	M.log = @(y, g) (g-y)';

    M.rand = @() randn(dimensions_vec);
    
    M.transp = @(x1, x2, d) d;
    M.isotransp = M.transp; % the transport is isometric

end
