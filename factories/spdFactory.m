function M = spdFactory(n)
    
    symm = @(X) .5*(X+X');
    
    M.name = @() sprintf('%dx%d Symmetric Positive Definite (SPD) matrices', n, n);
    
    M.dim = @() n*(n+1)/2;
    M.id = @() 'SPD';

    % Helpers to avoid computing full matrices simply to extract their trace
	vec     = @(A) A(:);
	trinner = @(A, B) vec(A')'*vec(B);  % = trace(A*B)
	trnorm  = @(A) sqrt(trinner(A, A)); % = sqrt(trace(A^2))
    % Choice of the metric on the orthonormal space is motivated by the
    % symmetry present in the space. The metric on the positive definite
    % cone is its natural bi-invariant metric.
	% The result is equal to: trace( (X\eta) * (X\zeta) )
    M.inner = @(X, eta, zeta) trinner(X\eta, X\zeta);
    % Notice that X\eta is *not* symmetric in general.
	% The result is equal to: sqrt(trace((X\eta)^2))
    % There should be no need to take the real part, but rounding errors
    % may cause a small imaginary part to appear, so we discard it.
    M.norm = @(X, eta) real(trnorm(X\eta));
    
    % Same here: X\Y is not symmetric in general.
    % Same remark about taking the real part.
    %M.dist = @(X, Y) real(trnorm(real(logm(X\Y))));
    M.dist = @distance;
    function d = distance(X, Y, method)
        if (nargin<3)||(isempty(method))
            method = 'logeuclid';
        end
        switch method
            case 'cholesky'
                % ref: I. L. Dryden, A. Koloydenko, and D. Zhou, 
                % “Non-Euclidean Statistics for Covari- ance Matrices, 
                % with Applications to Diffusion Tensor Imaging”, The 
                % Annals ofAp- plied Statistics, 2009.

                % Geodesic: No
                d = norm(chol(X) - chol(Y),'fro');
            case 'riemann'
                d = sqrt(sum(log(eig(X,Y)).^2));
            case 'kullback'
                % ref: Z. Wang and B. C. Vemuri, “An Affine Invariant 
                % Tensor Dissimilarity Measure and its Applications to 
                % Tensor-valued Image Segmentation”, In CVPR, 2004.

                % inv(X)*Y = X\Y
                % inv(Y)*X = X/Y
                d = 0.5* sqrt( trace( X\Y + X/Y - 2*eye(size(X))));
            case 'logeuclid'
                % Geodesic: Yes
                d = norm(logm(Y)-logm(X),'fro');
            case 'affine'
                %ref: X. Pennec, P. Fillard, and N. Ayache, “A Riemannian 
                % Framework for Tensor Com- puting”, IJCV, 2006.

                % Geodesic: Yes
                X12 = X^(-0.5);
                d = norm(logm(X12 * Y * X12),'fro');
            case 'opttransp'
                X12 = X^(0.5);
                d = sqrt(trace(X) + trace(Y) - 2 * trace((X12*Y*X12)^0.5));
            case 'logdet'
                % ref: A. Cherian, S. Sra, A. Banerjee, and N. 
                % Papanikolopoulos, “Efficient Similarity Search for 
                % Covariance Matrices via the Jensen-Bregman LogDet 
                % Divergence”, In ICCV, 2011.

                % Geodesic: No
                d = sqrt(log(det((X+Y)/2)) - 0.5*log(det(X*Y)));
            otherwise
                error(['Error. Not valid distance argument for this ' ...
                    'geometry' '''' M.id() ''''])
        end        
        d = norm((logm(X)-logm(Y)),'fro');
    end
    M.exp = @exponential;
    function X = exponential(Y, V, dt)
        % Parameters:
        %   - V:        Mandel vector representation of Symmetric matrix on
        %               the tangent space of X   or d x N
        %   - Y:        Base SPD matrix
        % 
        % Returns:
        %   - X:        SPD matrix Exp_Y(V)
        %               or SPD matrices d x d x N
        if nargin < 3
            dt = 1.0;
        end
        H = vec2symMat(V)*dt;
    	[r,c,N] = size(H);
        X = zeros(r,c,N);
        for i = 1:N
            % 	X(:,:,n) = S^.5 * expm(S^-.5 * U(:,:,n) * S^-.5) * S^.5;
	        [v,d] = eig(real(Y\H(:,:,i)));
	        X(:,:,i) = Y * v*diag(exp(diag(d)))*v^-1;
        end
    end
    
    M.log = @logarithm;
    function V = logarithm(Y, X)
        % Parameters:
        %   - X:        SPD matrix
        %               or SPD matrices d x d x N
        %   - Y:        Base SPD matrix
        %
        % Returns:
        %   - U:        Symmetric matrix Log_Y(X)
        %               or symmetric matrices d x d x N
    	[r,c,N] = size(X);
        H = zeros(r,c,N);
    	for i = 1:N
    		[v,d] = eig(Y\X(:,:,i));
    		H(:,:,i) = Y * v*diag(log(diag(d)))*v^-1;
    	end
        V = symMat2Vec(H);
    end

    % Generate a random symmetric positive definite matrix following a
    % certain distribution. The particular choice of a distribution is of
    % course arbitrary, and specific applications might require different
    % ones.
    M.rand = @random;
    function X = random()
        D = diag(1+rand(n, 1));
        [Q, R] = qr(randn(n)); %#ok
        X = Q*D*Q';
    end    
    
    % For reference, a proper vector transport is given here, following
    % work by Sra and Hosseini: "Conic geometric optimisation on the
    % manifold of positive definite matrices", to appear in SIAM J. Optim.
    % in 2015; also available here: http://arxiv.org/abs/1312.1039
    % This will not be used by default. To force the use of this transport,
    % execute "M.transp = M.paralleltransp;" on your M returned by the
    % present factory.
    M.paralleltransp = @parallel_transport;
    function zeta = parallel_transport(X, Y, V)
        E = sqrtm((Y/X));
        zeta = E*V*E';
    end
   
end

function S = vec2symMat(V)
% Transforms matrix of vectors to tensor of symmetric matrices

[d, N] = size(V);
D = (-1 + sqrt(1 + 8*d))/2;
for n = 1:N
	v = V(:,n);
	M = diag(v(1:D));
	id = cumsum(fliplr(1:D));

	for i = 1:D-1
	  M = M + diag(v(id(i)+1:id(i+1)),i)./sqrt(2) + diag(v(id(i)+1:id(i+1)),-i)./sqrt(2); % Mandel notation
	%   M = M + diag(v(id(i+1)+1:id(i+1)),i) + diag(v(id(i+1)+1:id(i+1)),-i); % Voigt notation
	end
	S(:,:,n) = M;
end
end

function V = symMat2Vec(S)
% Vectorization of a tensor of symmetric matrix
[D, ~, N] = size(S);

V = [];
for n = 1:N
	v = [];
	v = diag(S(:,:,n));
	for d = 1:D-1
	  v = [v; sqrt(2).*diag(S(:,:,n),d)]; % Mandel notation
	%   v = [v; diag(M,n)]; % Voigt notation
	end
	V = [V v];
end

end