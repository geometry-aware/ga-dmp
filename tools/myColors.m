mycolors = struct('r', [180,20,47]/255, ...  %red
    'b', [0,114,189]/255, ... %blue
    'g', [119,172,48]/255, ... %green
	'o', [217,83,25]/255, ... % orange
	'y', [237,177,32]/255, ... % yellow
	'p', [126,47,142]/255, ... % purple
	'pi', [204,102,102]/255, ... % pink
	'lb', [77,190,238]/255, ... % light blue
	'li', [164,196,0]/255, ... % lime
	'lr', [229,20,0]/255, ... % light red
	'lg', [220,220,220]/255, ... % light gray
	'dr', [102,0,0]/255, ... % dark red
	'em', [0,138,0]/255, ... % emerald
    'br', [0.6510, 0.5725, 0.3412], ... %bronze
    'gy', [0.6, 0.6, 0.6],...     % gray
    'vgy', [160 160 160]/255,...       % between gray and dark
    'm',  [1 0 1],... % magenta
    'c',  [0 1 1],... % cyan
    'rr', [1.0 0.4 0.4],... % clear red
    'cg', [0 127 0]/255,... % clear green
    'cw', [212 85 0]/255,... % clear brown
    'cr', [170 0 0]/255,... % clear red
    'cb', [0 113 188]/255,... % clear blue
    'cy', [236 176 31]/255,... % clear yellow
    'gl', [0.8314, 0.7020, 0.7843] );   %greyed lavender