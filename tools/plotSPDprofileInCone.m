function plotSPDprofileInCone(M, S, r, varargin)

% setup input parser
p = inputParser;

addParameter(p, 'Parent', [], ...
    @(x)validateattributes(x, {'matlab.graphics.axis.Axes'}, {'nonempty', 'scalar'}, 'plotTransforms', 'Parent'));
addParameter(p, 'color', [], @(x)validateattributes(x, {'string', 'char', 'numeric'}, {'nonempty'}, 'color'));
addParameter(p, 'LineStyle', [], @(x)validateattributes(x, {'string', 'char'}, {'nonempty'}, 'LineStyle'));
addParameter(p, 'View', [], @(x)validateattributes(x, {'string', 'char', 'numeric'}, {'nonempty'}, 'plotTransforms', 'View'));
addParameter(p, 'LineWidth', [], @(x)validateattributes(x, {'numeric'}, {'scalar','positive'}, 'LineWidth'));

% parse inputs
parse(p, varargin{:})

% prepare geodesic color
if ~isempty(p.Results.color)
    color = p.Results.color;
else
    color = 'k';
end
% prepare line style
if ~isempty(p.Results.LineStyle)
    LineStyle = p.Results.LineStyle;
else
    LineStyle = '-';
end
% prepare line width
if ~isempty(p.Results.LineWidth)
    lW = p.Results.LineWidth;
else
    lW = 2;
end

if ~nargin
    r = 20;  % or whatever
end

nbData = length(S);
colorss = lines(20);

if iscell(S)
    [m,n] = size(S{1});
    tmp = zeros(m,n,nbData);
    for i = 1:nbData
        tmp(:,:,i) = S{i,1};
    end
    clear S
    S = tmp;
end
%% Prepare geodesics from S1 to S2
nbDrawingSeg = 40; %Number of segments used to draw ellipsoids
p_geo = [];
xyzMsh = [];
for i=1:nbData-1
    U = M.log(S(:,:,i+1),S(:,:,i));
    %umsh = bsxfun(@times,U,reshape(linspace(0,1,nbDrawingSeg),1,1,nbDrawingSeg));
    umsh = bsxfun(@times,U,reshape(linspace(0,1,nbDrawingSeg),1,nbDrawingSeg));
    msh = M.exp(S(:,:,i+1),umsh);
    xyzMsh = [xyzMsh; [reshape(msh(1,1,:),[nbDrawingSeg,1]),...
        reshape(msh(2,2,:),[nbDrawingSeg,1]), reshape(msh(2,1,:),...
        [nbDrawingSeg,1])]];   
end

%r = max(max(xyzMsh));
%% Plot SPD cone
% prepare ax for plotting
if isempty(p.Results.Parent)
    fig = newplot; %figure('position',[280 85 589 441],'color',[1 1 1]);
    set(gcf, 'color', [1 1 1])
    axis equal
    grid off
    parentAx = fig;
    hold on;
else
    parentAx = p.Results.Parent;
    hold on;
end
phi = 0:0.1:2*pi+0.1;
x = [zeros(size(phi)); r.*ones(size(phi))];
y = [zeros(size(phi));r.*sin(phi)];
z = [zeros(size(phi));r/sqrt(2).*cos(phi)];

h = mesh(x,y,z,'linestyle','none','facecolor',[.85 .85 .85],'facealpha',.7);
direction = cross([1 0 0],[1/sqrt(2),1/sqrt(2),0]);
rotate(h,direction,45,[0,0,0])

h = plot3(x(2,:),y(2,:),z(2,:),'linewidth',1,'color',[.8 .8 .8]);
rotate(h,direction,45,[0,0,0])
axis off
% Optionally change the view if the user has provided that input
if ~isempty(p.Results.View)
    viewAzEl = validateViewInput(p.Results.View);
    view(parentAx, viewAzEl(1), viewAzEl(2));
else
    view(70,12);
end


%% Plot datapoints on the manifold
x = reshape(S(1,1,:),1,nbData);
y = reshape(S(2,2,:),1,nbData);
z = reshape(S(2,1,:),1,nbData);
plot3(x, y, z, LineStyle,'linewidth',lW,'color',color);

% p_geo = [p_geo plot3(xyzMsh(:,1), xyzMsh(:,2), xyzMsh(:,3), ...
%     LineStyle,'linewidth',lW,'color',color)];

end

function selectedView = validateViewInput(inputViewValue)
%validateViewInput Validate the input the the "View" Name/Value pair
%   The earlier check verifies that the user-specified input is nonempty,
%   but the default value (no input provided) is empty. This function
%   converts the input options to a 2-element vector [AZ EL] containing the
%   azimuth and elevation angles that are passed to the view command
%   downstream.
%   Copyright 2018-2019 The MathWorks, Inc.

if isnumeric(inputViewValue) && ~isempty(inputViewValue)
    % If the input is numeric, extract azimuth and elevation angles
    validateattributes(inputViewValue, {'numeric'}, {'vector', 'numel', 2, 'finite'}, 'plotTransforms', 'View');
    selectedView = inputViewValue;
else
    % If the input is a string, it should specify 2D or 3D. Convert
    % these two azimuth and elevation angles
    viewStr = validatestring(inputViewValue, {'2D', '3D'}, 'plotTransforms', 'View');
    if strcmp(viewStr, '2D')
        [az, el] = view(2);
    else
        [az, el] = view(3);
    end
    selectedView = [az el];
end

end