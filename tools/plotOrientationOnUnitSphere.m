%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%PLOTQUATPROFILEONSPHERE plots unit quaternion trajectory on a Sphere
%
%   Input Arguments:
%   Q               Unit quaternion trajectory N x 4 matrix.
%   'FrameSize'     Positive numeric that determines thesize of the plotted
%                   transform frame and the mesh attached to the transform.
%   'View'          A string or 3-element vector indicating the desired
%                   plot view. Valid options are "2D", "3D", or the
%                   3-element vector [x y z] that sets the view angle in
%                   Cartesian coordinates. The magnitude of vector Z, Y, Z
%                   is ignored. The default value is "3D".
%	'Parent'        A handle to an axis upon which this plot would be
%                   drawn.
%	'dt'            The sample time, default is 0.01
%	'colormap'      Defines the colormap of the Sphere, defalt is 'gray'.
%	'nFrames'       Number of frame to be plotted, default is 0.
%	'color'         Geodesic color, default is 'k'
%	'LineWidth'     Geodesic width, default is 2.
%	'nBlochVector'	Number of Bloch vectors to be plotted, default is 0.
%
%   Example: Create a three dimensional array and then check for the
%            attribute '2d'.
%   plotQuatProfileOnSphere(Q, 'Framesize', 0.01, 'View', [45 0],...
%       'dt', 0.01, 'colormap', 'gray','nFrames',10, 'nBlochVector', 2,...
%       'color','r', 'LineWidth',2.9)%
%
% Copyright (C) 2013-2018, by Abu-Dakka, Fares J.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotOrientationOnUnitSphere(M,Q, varargin)

%% validate qyaternion trajectory Q
id = M.id();
switch id%elems{i}
    case 'SO'
        validateattributes(Q, {'numeric'}, {'3d', 'nrows', 3, 'ncols', 3, 'nonempty'});
        %% Obtain the interpolated quaternions.
        q = quaternion(Q, 'rotmat', 'frame');
    case 'S3'
        validateattributes(Q, {'numeric'}, {'2d', 'ncols', 4, 'nonempty'});
        %% Obtain the interpolated quaternions.
        q = quaternion(Q);
    otherwise
        disp('other value')
end

% setup input parser
p = inputParser;

% optional inputs
addParameter(p, 'FrameSize', 1, ...
    @(x)validateattributes(x, {'numeric'}, {'scalar','positive'}, 'FrameSize'));
addParameter(p, 'Parent', [], ...
    @(x)validateattributes(x, {'matlab.graphics.axis.Axes'}, {'nonempty', 'scalar'}, 'plotTransforms', 'Parent'));
% postpone full validation for view, string, file path, and mesh color
addParameter(p, 'View', [], @(x)validateattributes(x, {'string', 'char', 'numeric'}, {'nonempty'}, 'plotTransforms', 'View'));
addParameter(p, 'dt', [], @(x)validateattributes(x, {'numeric'}, {'nonempty'}));
addParameter(p, 'colormap', [], @(x)validateattributes(x, {'string', 'char'}, {'nonempty'}, 'colormap'));
addParameter(p, 'color', [], @(x)validateattributes(x, {'string', 'char', 'numeric'}, {'nonempty'}, 'color'));
addParameter(p, 'LineWidth', [], @(x)validateattributes(x, {'numeric'}, {'scalar','positive'}, 'LineWidth'));
addParameter(p, 'nFrames', [], @(x)validateattributes(x, {'numeric'}, {'scalar','>=',0}, 'nFrames'));
addParameter(p, 'nBlochVector', [], @(x)validateattributes(x, {'numeric'}, {'scalar','>=',0}, 'nBlochVector'));
addParameter(p, 'LineStyle', [], @(x)validateattributes(x, {'string', 'char'}, {'nonempty'}, 'LineStyle'));
addParameter(p, 'plotGoal', [], @(x)validateattributes(x, {'string', 'char'}, {'nonempty'}, 'plotGoal'));

% parse inputs
parse(p, varargin{:})

% prepare FrameSize
if ~isempty(p.Results.FrameSize)
    fSize = p.Results.FrameSize;
else
    fSize = 0.05;
end
% prepare dt
if ~isempty(p.Results.dt)
    dt = p.Results.dt;
else
    dt = 0.01;
end
% prepare number of frames
if ~isempty(p.Results.nFrames)
    nF = p.Results.nFrames;
else
    nF = 0;
end
% prepare number of Bloch Vector
if ~isempty(p.Results.nBlochVector)
    nBV = p.Results.nBlochVector;
else
    nBV = 0;
end
% prepare colormap
if ~isempty(p.Results.colormap)
    ColorMap = p.Results.colormap;
else
    ColorMap = 'gray';
end
% prepare geodesic color
if ~isempty(p.Results.color)
    color = p.Results.color;
else
    color = 'k';
end
% prepare line width
if ~isempty(p.Results.LineWidth)
    lW = p.Results.LineWidth;
else
    lW = 2;
end
% prepare line style
if ~isempty(p.Results.LineStyle)
    LineStyle = p.Results.LineStyle;
else
    LineStyle = '-';
end
% prepare plot goal
if ~isempty(p.Results.LineStyle)
    plotGoal = p.Results.plotGoal;
else
    plotGoal = 'off';
end

% prepare ax for plotting
if isempty(p.Results.Parent)
    fig = newplot;%figure('position',[280 85 589 441],'color',[1 1 1]);
    axis equal
    grid off
    axis off
    parentAx = fig;%.CurrentAxes;%
else
    parentAx = p.Results.Parent;
end
hold on;

%% Plot Unit Sphere
[kM,jM,iM] = sphere(50);
h = surf(kM,jM,iM);
set(h,'FaceAlpha',0.2)
set(h,'EdgeColor','none')
shading interp
colormap(fig,ColorMap);
axis equal

% Optionally change the view if the user has provided that input
if ~isempty(p.Results.View)
    viewAzEl = validateViewInput(p.Results.View);
    view(parentAx, viewAzEl(1), viewAzEl(2));
else
    view([160,25]);
end


%% Obtain the corresponding rotate points.
thetaPhi(:,1) = linspace(0+0.005,pi-0.005,1000);
thetaPhi(:,2) = linspace(0+0.005,2*pi-0.005,1000);
theta0 = thetaPhi(500,1);
phi0   = thetaPhi(1,2);
r0 = [cos(theta0);sin(theta0)*sin(phi0);sin(theta0)*cos(phi0)]';
%r0 = [1 0 0];
pts = rotatepoint(q,r0);

plot3(pts(:,1),pts(:,2),pts(:,3),'LineStyle',LineStyle,'color',color,'LineWidth',lW)

colMat = eye(3);
lineWidth = 2;

for kk = round(linspace(1,length(Q),nF))%1:length(Q)
    %% Plot frames
    %hp = plotTransforms([pts(kk,3),pts(kk,2),pts(kk,1)],...
    %    Q(kk,:),'FrameSize',fSize);hold on;
    R = quat2rotm( Q(kk,:) );
    %     h(1) = plot3([pts(kk,3) pts(kk,3)+R(3,1)], ...
    %         [pts(kk,2) pts(kk,2)+R(2,1)], ...
    %         [pts(kk,1) pts(kk,1)+R(1,1)], 'linewidth', lineWidth, 'color', colMat(:,1));
    %     h(2) = plot3([pts(kk,3) pts(kk,3)+R(3,2)], ...
    %         [pts(kk,2) pts(kk,2)+R(2,2)], ...
    %         [pts(kk,1) pts(kk,1)+R(1,2)], 'linewidth', lineWidth, 'color', colMat(:,2));
    %     h(3) = plot3([pts(kk,3) pts(kk,3)+R(3,3)], ...
    %         [pts(kk,2) pts(kk,2)+R(2,3)], ...
    %         [pts(kk,1) pts(kk,1)+R(1,3)], 'linewidth', lineWidth, 'color', colMat(:,3));

    h(1) = quiver3( pts(kk,3), pts(kk,2), pts(kk,1), R(3,1), R(2,1), R(1,1), 0.2, 'linewidth', 2, 'color', colMat(:,1));
    h(2) = quiver3( pts(kk,3), pts(kk,2), pts(kk,1), R(3,2), R(2,2), R(1,2), 0.2, 'linewidth', 2, 'color', colMat(:,2));
    h(3) = quiver3( pts(kk,3), pts(kk,2), pts(kk,1), R(3,3), R(2,3), R(1,3), 0.2, 'linewidth', 2, 'color', colMat(:,3));

end
%% Plot the Bloch Vector
for kk = round(linspace(1,length(Q),nF))%1:length(Q)
    quiver3(0,0,0,pts(kk,3),pts(kk,2),pts(kk,1),...
        'color',[1 1 1],'LineWidth',.1,'MaxHeadSize',0,'AutoScaleFactor',1)
end
%% Plot goal
if strcmp(plotGoal, 'on')
    pdata = plot3(pts(end,1), pts(end,2), pts(end,3), 'o',...
        'markersize',10,'MarkerFaceColor','k','color','k');
end
set(gca, 'FontSize', 15);

end

function selectedView = validateViewInput(inputViewValue)
%validateViewInput Validate the input the the "View" Name/Value pair
%   The earlier check verifies that the user-specified input is nonempty,
%   but the default value (no input provided) is empty. This function
%   converts the input options to a 2-element vector [AZ EL] containing the
%   azimuth and elevation angles that are passed to the view command
%   downstream.
%   Copyright 2018-2019 The MathWorks, Inc.

if isnumeric(inputViewValue) && ~isempty(inputViewValue)
    % If the input is numeric, extract azimuth and elevation angles
    validateattributes(inputViewValue, {'numeric'}, {'vector', 'numel', 2, 'finite'}, 'plotTransforms', 'View');
    selectedView = inputViewValue;
else
    % If the input is a string, it should specify 2D or 3D. Convert
    % these two azimuth and elevation angles
    viewStr = validatestring(inputViewValue, {'2D', '3D'}, 'plotTransforms', 'View');
    if strcmp(viewStr, '2D')
        [az, el] = view(2);
    else
        [az, el] = view(3);
    end
    selectedView = [az el];
end

end
