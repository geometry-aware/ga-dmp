demo_rotMat_02

rot_0_fig=figure('position',[347 -1079 1132 988]);hold on; view([127.8133   11.0665]);box on;
rot_0_fig.CurrentAxes.XAxis.Limits = [-0.1991    1.0000];
rot_0_fig.CurrentAxes.YAxis.Limits = [-0.1473    1.0000];
rot_0_fig.CurrentAxes.ZAxis.Limits = [-0.1951    1.0000];
axis equal; %axis tight;
set(gca, 'FontSize', 15);
xlabel('$x$', 'FontSize', 25, 'interpreter','latex');
ylabel('$y$', 'FontSize', 25, 'interpreter','latex');
zlabel('$z$', 'FontSize', 25, 'interpreter','latex');
rot_0_fig.CurrentAxes.TickLabelInterpreter = 'latex';

fps = 30;
writerObj1 = VideoWriter('rotMatrix.avi'); % Name it.
writerObj1.FrameRate = fps;	% How many frames per second.
writerObj1.Quality  = 100;	% Video Quality.
open(writerObj1);

for i=round(linspace(1,n,n/4))
    RR = XX(:,:,i);    err = (t(n-i+1)*90/n + 0.1);
    rr = mycolors.r * err;    gg = mycolors.g * err;    bb = mycolors.b * err;
    HP1(1) = quiver3( pos(i,1)*0, pos(i,2)*0, pos(i,3)*0, RR(1,1), RR(2,1), RR(3,1), 1, 'linewidth', 3, 'color', rr);
    HP1(2) = quiver3( pos(i,1)*0, pos(i,2)*0, pos(i,3)*0, RR(1,2), RR(2,2), RR(3,2), 1, 'linewidth', 3, 'color', gg);
    HP1(3) = quiver3( pos(i,1)*0, pos(i,2)*0, pos(i,3)*0, RR(1,3), RR(2,3), RR(3,3), 1, 'linewidth', 3, 'color', bb);
    axis([-0.1991    1.0000 -0.1473    1.0000 -0.1951    1.0000])
    frame = getframe(gcf);
    writeVideo(writerObj1, frame);
end
close(writerObj1);
