clear
close all;

load('DMP_pos_1.mat');

P = pos;
P_demo = P;
P_dmp = dmpState.pos';

R = load('Replayed_dmp_pick_manip_1.txt');
N = length(R);
samples = 100;
P_robot = R(1:samples:N,1:3);

dt = 0.001*samples;

% Robot trajectory
c_green = [0 127 0]/255;
c_brown = [212 85 0]/255;
c_red   = [170 0 0]/255;
c_blue  = [0 113 188]/255;
c_yello = [236 176 31]/255;

figure;
hold on; box on;
time_ = dt*(0:length(P)-1);
xlim([time_(1) time_(end)]);

plot(time_, P, 'k--')
plot(time_, P_dmp, 'k')
plot(time_, P_robot(:,1), 'color', c_brown)
plot(time_, P_robot(:,2), 'color', c_green)
plot(time_, P_robot(:,3), 'color', c_blue)

%% Manipulability
%clear;
close all;


addpath(genpath('../Water/dmp-spd'))

load('manipulability_pick.mat');

N = size(Me_d_all,3);
M_vec = zeros(6,N-1);
M_demo = Me_d_all(1:3,1:3,:);
for i=2:N
    M_vec(:,i-1) = symMat2Vec(Me_d_all(1:3,1:3,i));    
end
M_vec = [M_vec, M_vec(:,end)];
samples = 100;
dt = 0.001*samples;
time_ = dt*(0:N-1);

figure(1)
hold on; box on;
xlim([time_(1) time_(end)]);
plot(time_, M_vec', 'k--');

%% Create Panda robot
% Robot parameters
gripperLenght = 103.3;
panda = mdl_panda(gripperLenght);

Q = load('pick_manip_joint_trajectory_1.txt');
nPts = length(Q);
Q = Q(1:samples:nPts,:);
nPts = length(Q);
spaceDim = 3;
M_dmp_vec = [];
for i=1:nPts
    Jd = panda.jacob0(Q(i,:)');
    M_dmp(:,:,i) = Jd(1:spaceDim,:) * Jd(1:spaceDim,:)';
    M_dmp_vec = [M_dmp_vec, symMat2Vec(M_dmp(:,:,i))]; 
end
plot(time_, M_dmp_vec', 'k');

R = load('Replayed_dmp_pick_manip_1.txt');
N = length(R);
Qrep = R(1:samples:N,8:14);
N = length(Qrep);
M_rep_vec = [];
for i=1:N
    Jd = panda.jacob0(Qrep(i,:)');
    M_robot(:,:,i) = Jd(1:spaceDim,:) * Jd(1:spaceDim,:)';
    M_rep_vec = [M_rep_vec, symMat2Vec(M_robot(:,:,i))]; 
end

c_green = [0 127 0]/255;
c_brown = [212 85 0]/255;
c_red   = [170 0 0]/255;
c_blue  = [0 113 188]/255;
c_yello = [236 176 31]/255;

plot(time_, M_rep_vec(1,:), 'color', c_brown); %K_11
plot(time_, M_rep_vec(2,:), 'color', c_green); %K_22
plot(time_, M_rep_vec(3,:), 'color', c_blue); %K_33
plot(time_, M_rep_vec(4,:), 'color', c_yello); %K_12
plot(time_, M_rep_vec(5,:), 'm'); %K_23
plot(time_, M_rep_vec(6,:), 'color', c_red); %K_13