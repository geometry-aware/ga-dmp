clear all
close all

clc
 
addpath(genpath('factories'));
addpath(genpath('tools'));
 
%% Load data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rawData = load('data/dataset_MAN/GShape_MAN.mat');
variables=fields(rawData);
Sorig = rawData.(variables{1}).demos{1}.quat;
pos = rawData.(variables{1}).demos{1}.tsPos';
n = length(Sorig);
myColors;

%% Fit S3 data using piecewise geodesic interpolating
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
M = s3Factory();
tau = 10;
[Y, dy, ddy, t] = interpolationUQ(M, Sorig, tau);

%% SET DMP parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dt=t(2)-t(1);
 
N = 50;
alpha_qGoal = 0.5;
alpha_z = 48/3;
alpha_x = 2;
alpha_pPos = 400;
alpha_pOri = 4;
DOF = M.dim();
y0 = Y{1};
yg = Y{end};
tau = 10;

%% Initialize DMP object
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp(['---DMP for ' M.name() '---']);
DMP = riemanDMP(M, DOF, N, alpha_z,alpha_x,tau,dt,y0,yg);
 
%% DMP estimate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
DMP = learn(DMP, Y, dy, ddy);
DMP.w 
 
%% Initial states
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
duration = 0;

S.y = DMP.y;
S.g = DMP.g;
S.gnew = DMP.g;
goalSind = 2*n;
 
% Goal switching
goal_switching = 0;
if goal_switching
    newGoal = double(UnitQuaternion([0.7 0.35 0.5 0.3]));
    goalSind = 500;
end

Q = zeros(round((DMP.tau-duration)/DMP.dt), 4);
Q(1,:) = S.y;

dQ = zeros(round((DMP.tau-duration)/DMP.dt), 3);
S.z =  zeros(3,1);%dy(:,1); 
dQ(1,:) = S.z;

ddQ = zeros(round((DMP.tau-duration)/DMP.dt), 3);
S.zd = zeros(3,1);%ddy(:,1);
ddQ(1,:) = S.zd;

x = zeros(round((DMP.tau-duration)/DMP.dt)+1, 1);
S.x = 1;
x(1) = 1;

time = zeros(round((DMP.tau-duration)/DMP.dt), 1);

FX = zeros(round((DMP.tau-duration)/DMP.dt)+1, M.dim());
PSI = zeros(round((DMP.tau-duration)/DMP.dt)+1,N);

i = 2;
 
%% Run DMP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
while S.x > exp(-DMP.alpha_x * (DMP.tau - duration + DMP.dt) / DMP.tau)
    if goal_switching && i == goalSind
        S.gnew = newGoal;
    end
    S = integration(DMP, S);
    x(i) = S.x;
    Q(i,:) = S.y;     % quaternions
    dQ(i,:) = S.z'/DMP.tau;             % angular velocities
    ddQ(i,:) = S.zd'/DMP.tau;            % angular acc
    FX(i,:) = S.f';
    PSI(i,:) = S.psi;
    time(i) = time(i-1) + DMP.dt;
    i = i + 1;
end
% quat_elem_fig = figure('position',[729 -609 241 119]);hold on;box on;
% p2 = plot(time,Q(:,1),'linewidth',2,'color',mycolors.cy);
% p3 = plot(time,Q(:,2),'linewidth',2,'color',mycolors.cb);
% p4 = plot(time,Q(:,3),'linewidth',2,'color',mycolors.cg);
% p5 = plot(time,Q(:,4),'linewidth',2,'color',mycolors.cw);
% p1 = plot(time,Sorig,'k-.','linewidth',2);
 
return
%% Plotting PSI
quat_psi_fig = figure('position',[729 -609 241 119]);hold on;
plot(time,PSI,'linewidth',1.5)
for ll = 1:length(PSI)
    wPSI(ll,:) = PSI(ll,:)'.*DMP.w(1,:)';
end
axis tight
ylabel('$\\Psi_i(x)$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
print(quat_psi_fig,'-dsvg','-r600','results/quat_psi_fig');
print(quat_psi_fig,'-dpdf','-r600','results/quat_psi_fig');
print(quat_psi_fig,'-dpng','-r600','results/quat_psi_fig');
%% Plotting wPSI
quat_wpsi_fig = figure('position',[729 -609 241 119]);hold on;
plot(time,wPSI,'linewidth',1.5)
axis tight
ylabel('$\w_i\Psi_i(x)$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
print(quat_wpsi_fig,'-dsvg','-r600','results/quat_wpsi_fig');
print(quat_wpsi_fig,'-dpdf','-r600','results/quat_wpsi_fig');
print(quat_wpsi_fig,'-dpng','-r600','results/quat_wpsi_fig');
 
%% Plotting SPD upertriangle elements
quat_elem_fig = figure('position',[729 -609 241 119]);hold on;box on;
p2 = plot(time,Q(:,1),'linewidth',2,'color',mycolors.cy);
p3 = plot(time,Q(:,2),'linewidth',2,'color',mycolors.cb);
p4 = plot(time,Q(:,3),'linewidth',2,'color',mycolors.cg);
p5 = plot(time,Q(:,4),'linewidth',2,'color',mycolors.cw);
p1 = plot(time,Sorig,'k-.','linewidth',2);
legend(p1(3),{'$\bcalQ^{demo}$'},'color','none')
%legend([p1(3) p2 p3 p4 p5],{'$\bcalQ^{demo}$','$\nu$','$u_x$','$u_y$','$u_z$'},'color','none')
ylabel('$\bcalQ$ elements','Fontsize',10)
xlabel('Time','Fontsize',10);
print(quat_elem_fig,'-dsvg','-r600','results/quat_elem_fig');
print(quat_elem_fig,'-dpdf','-r600','results/quat_elem_fig');
print(quat_elem_fig,'-dpng','-r600','results/quat_elem_fig');

%% Plot distances
dist_quat = zeros(round((DMP.tau-duration)/DMP.dt)+1, 1);
quat_dist_fig=figure('position',[729 -609 241 119]);hold on;box on;
for i=1:n
    dist_quat(i) = M.dist(Q(i,:),Sorig(:,i));
end
plot(time,dist_quat,'color',mycolors.b,'linewidth',1.5);%plot(d_KL);
ylabel('$error$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
set(gca,'Fontsize', 10);
print(quat_dist_fig,'-dsvg','-r600','results/quat_dist_fig');
print(quat_dist_fig,'-dpdf','-r600','results/quat_dist_fig');
print(quat_dist_fig,'-dpng','-r600','results/quat_dist_fig');

%% Plot stiffness over Cartesian
%pos = [demoUQ{1}.tsPos(1,:); demoUQ{1}.tsPos(2,:); demoUQ{1}.tsPos(3,:)]';   HP = [];
quat_cart_fig=figure('position',[758 -572 221 170]);hold on; view([-13.7935   62.1800]);box on;
plot3(pos(:,1), pos(:,2), pos(:,3),'linewidth',1.5,'color','k');
for i=round(linspace(1,n,n/4))
    R = quat2rotm( Sorig(:,i)' );
    HP1 = quiver3( pos(i,1), pos(i,2), pos(i,3), R(1,1), R(2,1), R(3,1), 5, 'linewidth', 1.5, 'color', [.7 .7 .7]);
    HP1 = quiver3( pos(i,1), pos(i,2), pos(i,3), R(1,2), R(2,2), R(3,2), 5, 'linewidth', 1.5, 'color', [.7 .7 .7]);
    HP1 = quiver3( pos(i,1), pos(i,2), pos(i,3), R(1,3), R(2,3), R(3,3), 5, 'linewidth', 1.5, 'color', [.7 .7 .7]);
end
for i=round(linspace(1,n,n/6))
    R = quat2rotm( Q(i,:) );
    HP1(2) = quiver3( pos(i,1), pos(i,2), pos(i,3), R(1,1), R(2,1), R(3,1), 5, 'linewidth', 3, 'color', mycolors.r);
    HP1(3) = quiver3( pos(i,1), pos(i,2), pos(i,3), R(1,2), R(2,2), R(3,2), 5, 'linewidth', 3, 'color', mycolors.g);
    HP1(4) = quiver3( pos(i,1), pos(i,2), pos(i,3), R(1,3), R(2,3), R(3,3), 5, 'linewidth', 3, 'color', mycolors.b);
end
axis equal; axis tight;
HP = [HP1(1) HP2(1)];
legText{1} = '$\bm{C}$';
legText{2} = '$\bm{\hat{C}}$';
if goal_switching
    [HP3, ~] = plot2x2elipsoids(pos(1:2,goalSind), .1*XX(:,:,goalSind),...
        [0 0 1], .4);
    legText{3} = 'Switching starts';
    [HP4, ~] = plot2x2elipsoids(pos(1:2,end), .1*S.gnew,...
        [1 0 0], .4);
    HP = [HP HP3(1) HP4(1)];
    legText{4} = 'New goal';
end
zlabel('$z$', 'Fontsize', 10);
ylabel('$y$', 'Fontsize', 10);
xlabel('$x$', 'Fontsize', 10);
set(gca, 'Fontsize', 10);
print(quat_cart_fig,'-dsvg','-r600','results/quat_cart_fig');
print(quat_cart_fig,'-dpdf','-r600','results/quat_cart_fig');
print(quat_cart_fig,'-dpng','-r600','results/quat_cart_fig');
h = legend(HP, legText, 'Location','northwest');
set(h, 'Fontsize', 10,'color','none');
clear HP HP1 HP2 HP3 HP4 legText

%% Plot Velocity
quat_Vel_fig=figure('position',[729 -609 241 119]);hold on;box on;
p2 = plot(time, dQ(:,1), 'color', mycolors.cw,'linewidth',2); %K_11
p3 = plot(time, dQ(:,3), 'color', mycolors.cy,'linewidth',2); %K_12
p4 = plot(time, dQ(:,2), 'color', mycolors.cg,'linewidth',2); %K_22
p1 = plot(time, dy','k-.','linewidth',2);
h = legend([p1(3) p2],'$\bcalW^{demo}$','colored $\bcalW^{dmp}$', 'color','none');
%h = legend([p1(3) p2 p3 p4],'$\bm{\omega}^{demo}$','$\omega_x$','$\omega_y$','$\omega_z$');
set(h, 'Fontsize', 10, 'Location','northwest', 'color','none');
ylabel('$\bcalW$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
axis tight;
set(gca, 'Fontsize', 9);
print(quat_Vel_fig,'-dsvg','-r600','results/quat_Vel_fig');
print(quat_Vel_fig,'-dpdf','-r600','results/quat_Vel_fig');
print(quat_Vel_fig,'-dpng','-r600','results/quat_Vel_fig');

%% Plot acceleration
quat_acc_fig=figure('position',[729 -609 241 119]);hold on;box on;
p1 = plot(time, ddy','k--','linewidth',2);
p2 = plot(time, ddQ(:,1), 'color', mycolors.cw,'linewidth',1.5); %K_11
p3 = plot(time, ddQ(:,3), 'color', mycolors.cy,'linewidth',1.5); %K_12
p4 = plot(time, ddQ(:,2), 'color', mycolors.cg,'linewidth',1.5); %K_22
h = legend([p1(3) p2 p3 p4],'$\bm{\dot{\omega}}_{demo}$','$\dot{\omega}_x$','$\dot{\omega}_y$','$\dot{\omega}_z$');
set(h, 'Fontsize', 10, 'Location','northwest', 'color','none');
ylabel('$\bm{\dot{\omega}}$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
axis([0, time(end), -.6, .6]);
set(gca, 'Fontsize', 9);
print(quat_acc_fig,'-dsvg','-r600','results/v');
print(quat_acc_fig,'-dpdf','-r600','results/quat_acc_fig');
print(quat_acc_fig,'-dpng','-r600','results/quat_acc_fig');

%% Plot S3 in sphere
quat_sphere_fig = figure;
pG = 'on';
plotQuatProfileOnUSphere(Q, 'Framesize', 0.01,...
    'View', [273.7835, 65.3944],'dt', 0.1, 'colormap', 'gray',...
    'nFrames',0, 'nBlochVector', 0,...
    'color',mycolors.cw, 'LineWidth',3)
plotQuatProfileOnUSphere(Sorig', 'Framesize', 0.5,...
    'View', [160,25],'dt', 0.1, 'colormap', 'gray',...
    'nFrames',0, 'nBlochVector', 0,...
    'plotGoal',pG,'color','k', 'LineWidth',4,'LineStyle','-.')
legend('','$\bm{q}_{dmp}$','','$\bm{q}_{demo}$','$\G_q$','')
print(quat_sphere_fig,'-dsvg','-r600','results/quat_sphere_fig');
print(quat_sphere_fig,'-dpdf','-r600','results/quat_sphere_fig');
print(quat_sphere_fig,'-dpng','-r600','results/quat_sphere_fig');
