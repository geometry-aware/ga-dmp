classdef spdClass
    properties
        n
        dim
        data
    end
    methods
        function obj = spdClass(N)
            obj.dim = N*(N+1)/2;
            obj.n = N;
            obj.data = eye(N);
        end
        function name(obj)
            sprintf('Symmetric positive definite space of %dx%d matrices', obj.n, obj.n);
        end
        function Y = exp(obj,U)
            % The symm() and real() calls are mathematically not necessary 
            % but are numerically necessary.
            symm = @(X) .5*(X+X');
            Y = symm(obj.data*real(expm(obj.data\U)));
        end
        function H = log(obj,Y)
            % The symm() and real() calls are mathematically not necessary 
            % but are numerically necessary.
            H = symm(obj.data*real(logm(obj.data\Y)));
        end
        function obj = random(obj)
            D = diag(1+rand(obj.n, 1));
            [Q, R] = qr(randn(obj.n)); %#ok
            obj.data = Q*D*Q';
        end
    end
end
