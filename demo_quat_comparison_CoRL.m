clear all
close all

clc
 
addpath(genpath('factories'));
addpath(genpath('tools'));
 
%% Load data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rawData = load('data/data_orientation.dat');
%variables=fields(rawData);
Sorig = rawData(:,2:end)';
for i = 2:length(Sorig)  
   if (Sorig(:, i-1)' * Sorig(:, i) < 0)
       Sorig(:, i) = -Sorig(:, i);
   end
end

%pos = rawData.(variables{1}).demos{1}.tsPos';
n = length(Sorig);
myColors;

%% Fit S3 data using piecewise geodesic interpolating
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
M = s3Factory();
tau = rawData(end,1);
[Y, dy, ddy, t] = interpolationUQ(M, Sorig, tau);

%% SET DMP parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dt=t(2)-t(1);
 
N = 60;
alpha_qGoal = 0.5;
alpha_z = 15;
alpha_x = 4.6052;
alpha_pPos = 400;
alpha_pOri = 4;
DOF = M.dim();
y0 = Y{1};
yg = Y{end};

%% Initialize DMP object
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp(['---DMP for ' M.name() '---']);
DMP = riemanDMP(M, DOF, N, alpha_z,alpha_x,tau,dt,y0,yg);
 
 
%% DMP estimate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
DMP = learn(DMP, Y, dy, ddy);
 
%% Initial states
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
duration = 0;

S.y = DMP.y;
S.g = DMP.g;
S.gnew = DMP.g;
goalSind = 2*n;
 
% Goal switching
goal_switching = 0;
if goal_switching
    newGoal = double(UnitQuaternion([0.7442 0.5414 -0.0343 0.3897]));
    goalSind = 2;
end

Q = zeros(round((DMP.tau-duration)/DMP.dt), 4);
Q(1,:) = S.y;

dQ = zeros(round((DMP.tau-duration)/DMP.dt), 3);
S.z =  zeros(3,1);%dy(:,1); 
dQ(1,:) = S.z;

ddQ = zeros(round((DMP.tau-duration)/DMP.dt), 3);
S.zd = zeros(3,1);%ddy(:,1);
ddQ(1,:) = S.zd;

x = zeros(round((DMP.tau-duration)/DMP.dt), 1);
S.x = 1;
x(1) = 1;

time = zeros(round((DMP.tau-duration)/DMP.dt), 1);

FX = zeros(round((DMP.tau-duration)/DMP.dt), M.dim());
PSI = zeros(round((DMP.tau-duration)/DMP.dt),N);

eQdemo = zeros(round((DMP.tau-duration)/DMP.dt), 3);
eQ = zeros(round((DMP.tau-duration)/DMP.dt), 3);
eQ(1,:) = M.log(S.y, Sorig(:,end))';

%compute  error
for i = 1:length(eQdemo)
  eQdemo(i,:) = M.log(Sorig(:,i),Sorig(:,end))';
end
  eQgd(1,:) = M.log(Sorig(:,1), S.g)';

i = 2;
 
%% Run DMP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
while S.x > exp(-DMP.alpha_x * (DMP.tau - duration + DMP.dt) / DMP.tau)
    if goal_switching && i == goalSind
        S.gnew = newGoal;
    end
    S = integration(DMP, S);
    x(i) = S.x;
    Q(i,:) = S.y;     % quaternions
    dQ(i,:) = S.z'/DMP.tau;             % angular velocities
    ddQ(i,:) = S.zd'/DMP.tau;            % angular acc
    FX(i,:) = S.f';
    PSI(i,:) = S.psi;
    time(i) = time(i-1) + DMP.dt;
    eQ(i,:) = M.log(S.y, Sorig(:,end))';
    eQgd(i,:) = M.log(Sorig(:,i),S.g)';
    i = i + 1;
end
labelsQ = ["$Q_\eta$", "$Q_{\epsilon_X}$", "$Q_{\epsilon_Y}$", "$Q_{\epsilon_Z}$"];
labelseQ = ["$e\bcalW_X [rad]$", "$e\bcalW_Y [rad]$", "$e\bcalW_Z [rad]$"];
labelsdeQ = ["$\dot{e}Q_X [rad/s]$", "$\dot{e}Q_Y [rad/s]$", "$\dot{e}Q_Z [rad/s]$"];
labelsomega = ["$\omega_X [rad/s]$", "$\omega_Y [rad/s]$", "$\omega_Z [rad/s]$"];

labelsDist = ["$u_{Dist, X} [Nm]$", "$u_{Dist, Y} [Nm]$", "$u_{Dist, Z} [Nm]$"];
len = min(length(Sorig),min(length(time)));
eqplot = figure('position',[404 -829 339 332]);hold on;
for i = 1:3
    subplot(3, 1, i);hold on
    plot(time(1:len), eQ(1:len, i), 'color',mycolors.cr, 'LineWidth', 2);
    plot(t(1:len), eQdemo(1:len, i), 'k--', 'LineWidth', 2);
    ylabel(labelseQ(i), 'fontsize', 14);
    if i == 3
        legend({'$e_{\bcalQ}^{dmp}$', '$e_{\bcalQ}^{demo}$'}, 'fontsize', 10); %'$y_{robot}$',
        xlabel('$t [s]$', 'Interpreter', 'latex', 'fontsize', 14);
    end 
    box on;
end
print(eqplot,'-dpdf','-r600','results/eqplot');
hold off;

load CoRLpaper.mat;
len = min(length(eQData),min(length(time)));

eqplotERR1 = figure('position',[404 -829 339 332]);hold on;
plot(eQdemo(1:len,1)-eQ(1:len,1), 'color',mycolors.cr, 'LineWidth', 2);
plot(eQdemo(1:len,2)-eQ(1:len,2), 'color',mycolors.cr, 'LineWidth', 2);
plot(eQdemo(1:len,3)-eQ(1:len,3), 'color',mycolors.cr, 'LineWidth', 2);
plot(eQd(1,1:len)'-eQData(1,1:len)','k--', 'LineWidth', 2); 
plot(eQd(2,1:len)'-eQData(2,1:len)','k--', 'LineWidth', 2); 
plot(eQd(3,1:len)'-eQData(3,1:len)','k--', 'LineWidth', 2); 
for i = 1:len
    NORM(i,1) = norm(eQdemo(i,:)-eQ(i,:));
    NORM(i,2) = norm(eQd(:,i)-eQData(:,i));
end
eqplotERR1 = figure('position',[404 -829 339 332]);hold on;box on
plot(time(1:len),NORM(:,1), 'color',mycolors.cr, 'LineWidth', 2);
plot(time(1:len),NORM(:,2),'k--', 'LineWidth', 2); 
print(eqplotERR1,'-dpdf','-r600','results/eqplotERR1');

eqplotERR = figure('position',[404 -829 339 332]);hold on;
subplot(3, 1, 1);hold on;box on
plot(time(1:len), eQ(1:len, 1), 'color',mycolors.cr, 'LineWidth', 2);
plot(t(1:len), eQData(1,1:len), 'k--', 'LineWidth', 2);
subplot(3, 1, 2);hold on;box on
plot(time(1:len), eQ(1:len, 2), 'color',mycolors.cr, 'LineWidth', 2);
plot(t(1:len), eQData(2,1:len), 'k--', 'LineWidth', 2);
subplot(3, 1, 3);hold on;box on
plot(time(1:len), eQ(1:len, 3), 'color',mycolors.cr, 'LineWidth', 2);
plot(t(1:len), eQData(3,1:len), 'k--', 'LineWidth', 2);
print(eqplotERR,'-dpdf','-r600','results/eqplotERR');
 
quat_elem_fig = figure('position',[375 -845 626 389]);hold on;box on;
subplot(4, 1, 1);hold on;box on;
p2 = plot(time,Q(:,1),'linewidth',2,'color',mycolors.cy);legend('1')
p1 = plot(time(1:len),Sorig(1,1:len),'k--','linewidth',2);
subplot(4, 1, 2);hold on;box on;
p3 = plot(time,Q(:,2),'linewidth',2,'color',mycolors.cb);legend('1')
p1 = plot(time(1:len),Sorig(2,1:len),'k--','linewidth',2);
subplot(4, 1, 3);hold on;box on;
p4 = plot(time,Q(:,3),'linewidth',2,'color',mycolors.cg);legend('1')
p1 = plot(time(1:len),Sorig(3,1:len),'k--','linewidth',2);
subplot(4, 1, 4);hold on;box on;
p5 = plot(time,Q(:,4),'linewidth',2,'color',mycolors.cw);legend('1')
p1 = plot(time(1:len),Sorig(4,1:len),'k--','linewidth',2);
print(quat_elem_fig,'-dpdf','-r600','results/quat_elem_fig');

dist_quat = zeros(len, 1);
quat_dist_fig=figure('position',[729 -609 241 119]);hold on;box on;
for i=1:len
    dist_quat(i) = M.dist(Q(i,:),Sorig(:,i));
end
plot(time(1:len),dist_quat,'color',mycolors.b,'linewidth',1.5);%plot(d_KL);

return
%% Plotting PSI
quat_psi_fig = figure('position',[729 -609 241 119]);hold on;
plot(time,PSI,'linewidth',1.5)
for ll = 1:length(PSI)
    wPSI(ll,:) = PSI(ll,:)'.*DMP.w(1,:)';
end
axis tight
ylabel('$\\Psi_i(x)$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
print(quat_psi_fig,'-dsvg','-r600','results/quat_psi_fig');
print(quat_psi_fig,'-dpdf','-r600','results/quat_psi_fig');
print(quat_psi_fig,'-dpng','-r600','results/quat_psi_fig');
%% Plotting wPSI
quat_wpsi_fig = figure('position',[729 -609 241 119]);hold on;
plot(time,wPSI,'linewidth',1.5)
axis tight
ylabel('$\w_i\Psi_i(x)$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
print(quat_wpsi_fig,'-dsvg','-r600','results/quat_wpsi_fig');
print(quat_wpsi_fig,'-dpdf','-r600','results/quat_wpsi_fig');
print(quat_wpsi_fig,'-dpng','-r600','results/quat_wpsi_fig');
 
%% Plotting SPD upertriangle elements
quat_elem_fig = figure('position',[729 -609 241 119]);hold on;box on;
p2 = plot(time,Q(:,1),'linewidth',2,'color',mycolors.cy);
p3 = plot(time,Q(:,2),'linewidth',2,'color',mycolors.cb);
p4 = plot(time,Q(:,3),'linewidth',2,'color',mycolors.cg);
p5 = plot(time,Q(:,4),'linewidth',2,'color',mycolors.cw);
p1 = plot(time,Sorig,'k-.','linewidth',2);
legend(p1(3),{'$\bcalQ^{demo}$'},'color','none')
%legend([p1(3) p2 p3 p4 p5],{'$\bcalQ^{demo}$','$\nu$','$u_x$','$u_y$','$u_z$'},'color','none')
ylabel('$\bcalQ$ elements','Fontsize',10)
xlabel('Time','Fontsize',10);
print(quat_elem_fig,'-dsvg','-r600','results/quat_elem_fig');
print(quat_elem_fig,'-dpdf','-r600','results/quat_elem_fig');
print(quat_elem_fig,'-dpng','-r600','results/quat_elem_fig');

%% Plot distances
dist_quat = zeros(round((DMP.tau-duration)/DMP.dt)+1, 1);
quat_dist_fig=figure('position',[729 -609 241 119]);hold on;box on;
for i=1:n
    dist_quat(i) = M.dist(Q(i,:),Sorig(:,i));
end
plot(time,dist_quat,'color',mycolors.b,'linewidth',1.5);%plot(d_KL);
ylabel('$error$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
set(gca,'Fontsize', 10);
print(quat_dist_fig,'-dsvg','-r600','results/quat_dist_fig');
print(quat_dist_fig,'-dpdf','-r600','results/quat_dist_fig');
print(quat_dist_fig,'-dpng','-r600','results/quat_dist_fig');

%% Plot stiffness over Cartesian
%pos = [demoUQ{1}.tsPos(1,:); demoUQ{1}.tsPos(2,:); demoUQ{1}.tsPos(3,:)]';   HP = [];
quat_cart_fig=figure('position',[758 -572 221 170]);hold on; view([-13.7935   62.1800]);box on;
plot3(pos(1,:), pos(2,:), pos(3,:),'linewidth',1.5,'color','k');
%plot(pos(:,1), pos(:,3),'linewidth',1.5,'color','k');
for i=round(linspace(1,n,n/4))
    R = quat2rotm( Sorig(:,i)' );
    HP1 = quiver3( pos(i,1), pos(i,2), pos(i,3), R(1,1), R(2,1), R(3,1), 5, 'linewidth', 1.5, 'color', [.7 .7 .7]);
    HP1 = quiver3( pos(i,1), pos(i,2), pos(i,3), R(1,2), R(2,2), R(3,2), 5, 'linewidth', 1.5, 'color', [.7 .7 .7]);
    HP1 = quiver3( pos(i,1), pos(i,2), pos(i,3), R(1,3), R(2,3), R(3,3), 5, 'linewidth', 1.5, 'color', [.7 .7 .7]);
end
for i=round(linspace(1,n,n/6))
    R = quat2rotm( Q(i,:) );
    HP1(2) = quiver3( pos(i,1), pos(i,2), pos(i,3), R(1,1), R(2,1), R(3,1), 5, 'linewidth', 3, 'color', mycolors.r);
    HP1(3) = quiver3( pos(i,1), pos(i,2), pos(i,3), R(1,2), R(2,2), R(3,2), 5, 'linewidth', 3, 'color', mycolors.g);
    HP1(4) = quiver3( pos(i,1), pos(i,2), pos(i,3), R(1,3), R(2,3), R(3,3), 5, 'linewidth', 3, 'color', mycolors.b);
end
axis equal; axis tight;
HP = [HP1(1) HP2(1)];
legText{1} = '$\bm{C}$';
legText{2} = '$\bm{\hat{C}}$';
if goal_switching
    [HP3, ~] = plot2x2elipsoids(pos(1:2,goalSind), .1*XX(:,:,goalSind),...
        [0 0 1], .4);
    legText{3} = 'Switching starts';
    [HP4, ~] = plot2x2elipsoids(pos(1:2,end), .1*S.gnew,...
        [1 0 0], .4);
    HP = [HP HP3(1) HP4(1)];
    legText{4} = 'New goal';
end
zlabel('$z$', 'Fontsize', 10);
ylabel('$y$', 'Fontsize', 10);
xlabel('$x$', 'Fontsize', 10);
set(gca, 'Fontsize', 10);
print(quat_cart_fig,'-dsvg','-r600','results/quat_cart_fig');
print(quat_cart_fig,'-dpdf','-r600','results/quat_cart_fig');
print(quat_cart_fig,'-dpng','-r600','results/quat_cart_fig');
h = legend(HP, legText, 'Location','northwest');
set(h, 'Fontsize', 10,'color','none');
clear HP HP1 HP2 HP3 HP4 legText

%% Plot Velocity
quat_Vel_fig=figure('position',[729 -609 241 119]);hold on;box on;
p2 = plot(time, dQ(:,1), 'color', mycolors.cw,'linewidth',2); %K_11
p3 = plot(time, dQ(:,3), 'color', mycolors.cy,'linewidth',2); %K_12
p4 = plot(time, dQ(:,2), 'color', mycolors.cg,'linewidth',2); %K_22
p1 = plot(time, dy','k-.','linewidth',2);
h = legend([p1(3) p2],'$\bcalW^{demo}$','colored $\bcalW^{dmp}$', 'color','none');
%h = legend([p1(3) p2 p3 p4],'$\bm{\omega}^{demo}$','$\omega_x$','$\omega_y$','$\omega_z$');
set(h, 'Fontsize', 10, 'Location','northwest', 'color','none');
ylabel('$\bcalW$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
axis tight;
set(gca, 'Fontsize', 9);
print(quat_Vel_fig,'-dsvg','-r600','results/quat_Vel_fig');
print(quat_Vel_fig,'-dpdf','-r600','results/quat_Vel_fig');
print(quat_Vel_fig,'-dpng','-r600','results/quat_Vel_fig');

%% Plot acceleration
quat_acc_fig=figure('position',[729 -609 241 119]);hold on;box on;
p1 = plot(time, ddy','k--','linewidth',2);
p2 = plot(time, ddQ(:,1), 'color', mycolors.cw,'linewidth',1.5); %K_11
p3 = plot(time, ddQ(:,3), 'color', mycolors.cy,'linewidth',1.5); %K_12
p4 = plot(time, ddQ(:,2), 'color', mycolors.cg,'linewidth',1.5); %K_22
h = legend([p1(3) p2 p3 p4],'$\bm{\dot{\omega}}_{demo}$','$\dot{\omega}_x$','$\dot{\omega}_y$','$\dot{\omega}_z$');
set(h, 'Fontsize', 10, 'Location','northwest', 'color','none');
ylabel('$\bm{\dot{\omega}}$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
axis([0, time(end), -.6, .6]);
set(gca, 'Fontsize', 9);
print(quat_acc_fig,'-dsvg','-r600','results/v');
print(quat_acc_fig,'-dpdf','-r600','results/quat_acc_fig');
print(quat_acc_fig,'-dpng','-r600','results/quat_acc_fig');

%% Plot S3 in sphere
quat_sphere_fig = figure;
pG = 'on';
plotQuatProfileOnUSphere(Q, 'Framesize', 0.01,...
    'View', [273.7835, 65.3944],'dt', 0.1, 'colormap', 'gray',...
    'nFrames',0, 'nBlochVector', 0,...
    'color',mycolors.cw, 'LineWidth',3)
plotQuatProfileOnUSphere(Sorig', 'Framesize', 0.5,...
    'View', [160,25],'dt', 0.1, 'colormap', 'gray',...
    'nFrames',0, 'nBlochVector', 0,...
    'plotGoal',pG,'color','k', 'LineWidth',4,'LineStyle','-.')
legend('','$\bm{q}_{dmp}$','','$\bm{q}_{demo}$','$\G_q$','')
print(quat_sphere_fig,'-dsvg','-r600','results/quat_sphere_fig');
print(quat_sphere_fig,'-dpdf','-r600','results/quat_sphere_fig');
print(quat_sphere_fig,'-dpng','-r600','results/quat_sphere_fig');
