clear all
close all
clc

%% Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nbData = 200; % Number of datapoints
nbSamples = 1; % Number of demonstrations
nbIter = 5; % Number of iteration for the Gauss Newton algorithm
nbIterEM = 5; % Number of iteration for the EM algorithm

%% Generate covariance data from handwriting data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
LASA = 1;
PLOT = 0;
if LASA
    % S. M. Khansari-Zadeh and A. Billard, "Learning Stable Non-Linear
    % Dynamical Systems with Gaussian Mixture Models", IEEE Transaction on
    % Robotics, 2011.
    p_path = './data/LASA_hand_writing_dataset/DataSet/';
    names = {'Angle','BendedLine','CShape','GShape', 'heee','JShape',...
        'Khamesh','LShape','NShape','PShape','RShape','Saeghe','Sharpc',...
        'Sine','Snake','Spoon','Sshape','Trapezoid','Worm','WShape',...
        'Zshape','JShape_2', 'Leaf_1', 'Leaf_2', 'DoubleBendedLine',...
        'Line','Multi_Models_1','Multi_Models_2','Multi_Models_3',...
        'Multi_Models_4'};
    dt = 0.0025;
    scale = 100;
    nbSamples0 = 7;
    nbStates = 30;
else
    % Calinon S. Gaussians on Riemannian manifolds: Applications for robot
    % learning and adaptive control. IEEE Robotics & Automation Magazine.
    % 2020 Apr 6;27(2):33-45.
    p_path = 'data/2Dletters/';
    names = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N',...
        'O','P','Q','R','S','T','U','V','W','X','Y','Z'};
    dt = 0.01;
    scale = 50;
    nbSamples0 = 10;
    nbStates = 10;
end
%% Start
for l = 4%1:length(names)
    load([p_path names{l} '.mat']);
    nbData = length(demos{1}.pos);
    xIn = [1:nbData] * dt;
    wOut=[];    Data=[];
    uIn=[]; uOut=[];
    for n=1:nbSamples0
        tmp.Data = spline(1:size(demos{n}.pos,2), demos{n}.pos, linspace(1,size(demos{n}.pos,2),nbData)); %Resampling
        Data = [Data [xIn; tmp.Data]];
        uOut = [uOut, [tmp.Data(1,:); tmp.Data]/scale];
        uIn = [uIn, [1:nbData]*dt];
    end

    %% Generate covariance data from handwriting data
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    mtmp.nbStates = nbStates;
    mtmp.nbVar = 3; % Dimension of the manifold and tangent space (1D input + 2^2 output)
    mtmp.nbVarCovOut = mtmp.nbVar + mtmp.nbVar*(mtmp.nbVar-1)/2; %Dimension of the output covariance
    mtmp.dt = dt; % Time step duration
    mtmp.params_diagRegFact = 1E-4;
    mtmp = init_GMM_kbins(Data, mtmp, nbSamples0);
    %mtmp = EM_GMM(Data, mtmp);
    [posOut, covOut] = GMR(mtmp, xIn, 1, 2:mtmp.nbVar);

    %% Generate artificial unit quaternion as output from handwriting data
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    xOut = expmap(uOut, [1; 0; 0; 0]);
    xIn = uIn;
    u = [uIn; uOut];
    x = [xIn; xOut];
    %% GMM parameters estimation (joint distribution with time as input, unit quaternion as output)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    model.nbStates = nbStates; %Number of states in the GMM
    model.nbVar = 4; %Dimension of the tangent space (incl. time)
    model.nbVarMan = 5; %Dimension of the manifold (incl. time)
    model.dt = dt; %Time step duration
    model.params_diagRegFact = 1E-4; %Regularization of covariance
    model.nbSamples = nbSamples0;
    model.nbIter = nbIter;
    model.nbIterEM = nbIterEM;
    model.nbData = nbData;
    [model, uTmp] = GMM_Q(xIn,xOut,u,model);


    xhat = GMR_Q(xIn,model);

    if PLOT
        spd_cart_fig=figure('position',[10 10 390 300]);hold on;
        for i=round(linspace(1,nbData,nbData/5))
            [HP1, ~] = plotGMM(posOut(1:2,i), covOut(:,:,i), [.7 .7 .7], .5); % Scaled matrix!
        end
        spd_Time_fig=figure('position',[10 10 450 170]);hold on;axis off;
        for i=round(linspace(1,nbData,nbData/5))
            [HP1, ~] = plotGMM([i/100;0], .0005*covOut(:,:,i), [.7 .7 .7], .5);
        end
        fig = figure;hold on;%figure('position',[280 85 589 441],'color',[1 1 1]);
        axis equal
        grid off
        axis off
        [kM,jM,iM] = sphere(50);
        h = surf(kM,jM,iM);
        set(h,'FaceAlpha',0.2)
        set(h,'EdgeColor','none')
        shading interp
        colormap(fig,'gray');
        axis equal
        view([160,25]);
        Q = xhat';
        q = quaternion(Q);
        r0 = [1 0 0];
        pts = rotatepoint(q,r0);
        plot3(pts(:,1),pts(:,2),pts(:,3),'LineStyle','-','color','k','LineWidth',2)

        %% Plot SPD cone
        % prepare ax for plotting
        fig = figure;  set(gcf, 'color', [1 1 1]);  axis equal
        grid off
        hold on;
        r = 20;
        phi = 0:0.1:2*pi+0.1;
        x = [zeros(size(phi)); r.*ones(size(phi))];
        y = [zeros(size(phi));r.*sin(phi)];
        z = [zeros(size(phi));r/sqrt(2).*cos(phi)];

        h = mesh(x,y,z,'linestyle','none','facecolor',[.85 .85 .85],'facealpha',.7);
        direction = cross([1 0 0],[1/sqrt(2),1/sqrt(2),0]);
        rotate(h,direction,45,[0,0,0])

        h = plot3(x(2,:),y(2,:),z(2,:),'linewidth',1,'color',[.8 .8 .8]);
        rotate(h,direction,45,[0,0,0])
        axis off
        view(70,12);

        plot3(reshape(covOut(1,1,:),1,nbData), reshape(covOut(2,2,:),1,nbData), reshape(covOut(2,1,:),1,nbData), '.','markersize',12,'color',[.5 .5 .5]);

    end
    %% Save new data
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    eval([names{l},'.cov = covOut']);
    eval([names{l},'.posOut = posOut']);
    eval([names{l},'.qutOut = xhat']);
    eval([names{l},'.demos = demos'])
    save(['./data/dataset_MAN/' names{l} '_MAN.mat'],names{l})
    clear mtmp model
end
return


%% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function x = expmap(u, mu)
x = QuatMatrix(mu) * expfct(u);
end

function u = logmap(x, mu)
if norm(mu-[1;0;0;0])<1e-6
    Q = [1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1];
else
    Q = QuatMatrix(mu);
end
u = logfct(Q'*x);
end

function Exp = expfct(u)
normv = sqrt(u(1,:).^2+u(2,:).^2+u(3,:).^2);
Exp = real([cos(normv) ; u(1,:).*sin(normv)./normv ; u(2,:).*sin(normv)./normv ; u(3,:).*sin(normv)./normv]);
Exp(:,normv < 1e-16) = repmat([1;0;0;0],1,sum(normv < 1e-16));
end

function Log = logfct(x)
% 	scale = acos(x(3,:)) ./ sqrt(1-x(3,:).^2);
scale = acoslog(x(1,:)) ./ sqrt(1-x(1,:).^2);
scale(isnan(scale)) = 1;
Log = [x(2,:).*scale; x(3,:).*scale; x(4,:).*scale];
end

function Q = QuatMatrix(q)
Q = [q(1) -q(2) -q(3) -q(4);
    q(2)  q(1) -q(4)  q(3);
    q(3)  q(4)  q(1) -q(2);
    q(4) -q(3)  q(2)  q(1)];
end

% Arcosine re-defitinion to make sure the distance between antipodal quaternions is zero (2.50 from Dubbelman's Thesis)
function acosx = acoslog(x)
for n=1:size(x,2)
    % sometimes abs(x) is not exactly 1.0
    if(x(n)>=1.0)
        x(n) = 1.0;
    end
    if(x(n)<=-1.0)
        x(n) = -1.0;
    end
    if(x(n)>=-1.0 && x(n)<0)
        acosx(n) = acos(x(n))-pi;
    else
        acosx(n) = acos(x(n));
    end
end
end

function Ac = transp(g,h)
E = [zeros(1,3); eye(3)];
vm = QuatMatrix(g) * [0; logmap(h,g)];
mn = norm(vm);
if mn < 1e-10
    disp('Angle of rotation too small (<1e-10)');
    Ac = eye(3);
    return;
end
uv = vm / mn;
Rpar = eye(4) - sin(mn)*(g*uv') - (1-cos(mn))*(uv*uv');
Ac = E' * QuatMatrix(h)' * Rpar * QuatMatrix(g) * E; %Transportation operator from g to h
end

function y = quat_exp(w, q)
if nargin == 1
    q = [1 0 0 0]';
end
for i = 1:size(w,2)
    nrm_w = norm(w(:,i), 'fro');
    if nrm_w > 0
        y(:,i) = [cos(nrm_w); w(:,i)*sin(nrm_w)/nrm_w];
        y(:,i) = y(:,i)/norm(y(:,i));
        y(:,i) = quat_multiply(y(:,i),q);
    else
        y(:,i) = [1 0 0 0];
    end
end
end
function log_q = quat_log(q1,q2)
n = size(q2,2);
log_q = zeros(3,n);
for i = 1:n
    q2c = quat_conjugate(q2(:,i));
    q = quat_multiply(q1, q2c);

    tmp = norm(q);
    q = q/tmp;

    if norm(q(2:4)) > 1.0e-12
        log_q(:,i) = 2* acos(q(1)) * q(2:4)' / norm(q(2:4));
    else
        log_q(:,i) = [0; 0; 0];
    end
end
end

function q = quat_conjugate(q)
q(2:4) = -q(2:4);
end
function q = quat_multiply(q1, q2)
if isrow(q1),   q1 = q1';   end
if isrow(q2),   q2 = q2';   end
q(1) = q1(1) * q2(1) - q1(2:4)' * q2(2:4);
q(2:4) = q1(1) * q2(2:4) + q2(1) * q1(2:4) + ...
    [q1(3)*q2(4) - q1(4)*q2(3); ...
    q1(4)*q2(2) - q1(2)*q2(4); ...
    q1(2)*q2(3) - q1(3)*q2(2)];
end

function X = spd_expmap(U,S)
% Exponential map (SPD manifold)
N = size(U,3);
for n = 1:N
	X(:,:,n) = S^.5 * expm(S^-.5 * U(:,:,n) * S^-.5) * S^.5;
end
end

function U = spd_logmap(X,S)
% Logarithm map 
N = size(X,3);
for n = 1:N
% 	U(:,:,n) = S^.5 * logm(S^-.5 * X(:,:,n) * S^-.5) * S^.5;
% 	U(:,:,n) = S * logm(S\X(:,:,n));
	[v,d] = eig(S\X(:,:,n));
	U(:,:,n) = S * v*diag(log(diag(d)))*v^-1;
end
end

function [model, uTmp] = GMM_Q(xIn,xOut,u,model)
%% GMM parameters estimation (joint distribution with time as input, unit quaternion as output)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
model = init_GMM_kbins(u, model, model.nbSamples);
model.MuMan = [model.Mu(1,:); expmap(model.Mu(2:end,:), [1; 0; 0; 0])]; %Center on the manifold %Data(1,nbData/2)
model.Mu = zeros(model.nbVar,model.nbStates); %Center in the tangent plane at point MuMan of the manifold

uTmp = zeros(model.nbVar,model.nbData*model.nbSamples,model.nbStates);
for nb=1:model.nbIterEM
    %E-step
    L = zeros(model.nbStates,size(u,2));
    for i=1:model.nbStates
        L(i,:) = model.Priors(i) * gaussPDF([xIn-model.MuMan(1,i); logmap(xOut, model.MuMan(2:end,i))], model.Mu(:,i), model.Sigma(:,:,i));
    end
    GAMMA = L ./ repmat(sum(L,1)+realmin, model.nbStates, 1);
    GAMMA2 = GAMMA ./ repmat(sum(GAMMA,2),1,model.nbData*model.nbSamples);
    %M-step
    for i=1:model.nbStates
        %Update Priors
        model.Priors(i) = sum(GAMMA(i,:)) / (model.nbData*model.nbSamples);
        %Update MuMan
        for n=1:model.nbIter
            uTmp(:,:,i) = [xIn-model.MuMan(1,i); logmap(xOut, model.MuMan(2:end,i))];
            model.MuMan(:,i) = [(model.MuMan(1,i)+uTmp(1,:,i))*GAMMA2(i,:)'; expmap(uTmp(2:end,:,i)*GAMMA2(i,:)', model.MuMan(2:end,i))];
        end
        %Update Sigma
        model.Sigma(:,:,i) = uTmp(:,:,i) * diag(GAMMA2(i,:)) * uTmp(:,:,i)' + eye(size(u,1)) * model.params_diagRegFact;
    end
end
end

function xhat = GMR_Q(xIn,model)
%% GMR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
in=1; out=2:4; outMan=2:5;
nbVarOut = length(out);
nbVarOutMan = length(outMan);

% %Adding artificial distortion and noise on the inputs
% xIn(:,1:nbData) = xIn(:,1:nbData) * 1.2 + randn(1,nbData)*1E-4;

uhat = zeros(nbVarOut,model.nbData);
xhat = zeros(nbVarOutMan,model.nbData);
uOut = zeros(nbVarOut,model.nbStates,model.nbData);
SigmaTmp = zeros(model.nbVar,model.nbVar,model.nbStates);
expSigma = zeros(nbVarOut,nbVarOut,model.nbData);

%Version with single optimization loop
for t=1:model.nbData
    %Compute activation weight
    for i=1:model.nbStates
        H(i,t) = model.Priors(i) * gaussPDF(xIn(:,t)-model.MuMan(in,i), model.Mu(in,i), model.Sigma(in,in,i));
    end
    H(:,t) = H(:,t) / sum(H(:,t)+realmin);

    %Compute conditional mean (with covariance transportation)
    if t==1
        [~,id] = max(H(:,t));
        xhat(:,t) = model.MuMan(outMan,id); %Initial point
    else
        xhat(:,t) = xhat(:,t-1);
    end
    for n=1:model.nbIter
        for i=1:model.nbStates
            %Transportation of covariance from model.MuMan(outMan,i) to xhat(:,t)
            Ac = transp(model.MuMan(outMan,i), xhat(:,t));
            SigmaTmp(:,:,i) = blkdiag(1,Ac) * model.Sigma(:,:,i) * blkdiag(1,Ac)';
            %Gaussian conditioning on the tangent space
            uOut(:,i,t) = logmap(model.MuMan(outMan,i), xhat(:,t)) + SigmaTmp(out,in,i)/SigmaTmp(in,in,i) * (xIn(:,t)-model.MuMan(in,i));
        end
        uhat(:,t) = uOut(:,:,t) * H(:,t);
        xhat(:,t) = expmap(uhat(:,t), xhat(:,t));
    end

    %Compute conditional covariances (note that since uhat=0, the final part in the GMR computation is dropped)
    for i=1:model.nbStates
        SigmaOutTmp = SigmaTmp(out,out,i) - SigmaTmp(out,in,i)/SigmaTmp(in,in,i) * SigmaTmp(in,out,i);
        expSigma(:,:,t) = expSigma(:,:,t) + H(i,t) * (SigmaOutTmp + uOut(:,i,t) * uOut(:,i,t)');
    end
end
end

function model = init_GMM_kbins(Data, model, nbSamples)
% Initialization of Gaussian Mixture Model (GMM) parameters by clustering 
% an ordered dataset into equal bins.
%
% Writing code takes time. Polishing it and making it available to others takes longer! 
% If some parts of the code were useful for your research of for a better understanding 
% of the algorithms, please reward the authors by citing the related publications, 
% and consider making your own research available in this way.
%
% @article{Calinon16JIST,
%   author="Calinon, S.",
%   title="A Tutorial on Task-Parameterized Movement Learning and Retrieval",
%   journal="Intelligent Service Robotics",
%   publisher="Springer Berlin Heidelberg",
%   doi="10.1007/s11370-015-0187-9",
%   year="2016",
%   volume="9",
%   number="1",
%   pages="1--29"
% }
% 
% Copyright (c) 2015 Idiap Research Institute, http://idiap.ch/
% Written by Sylvain Calinon, http://calinon.ch/
% 
% This file is part of PbDlib, http://www.idiap.ch/software/pbdlib/
% 
% PbDlib is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License version 3 as
% published by the Free Software Foundation.
% 
% PbDlib is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with PbDlib. If not, see <http://www.gnu.org/licenses/>.


%Parameters 
nbData = size(Data,2) / nbSamples;
if ~isfield(model,'params_diagRegFact')
	model.params_diagRegFact = 1E-4; %Optional regularization term to avoid numerical instability
end

%Delimit the cluster bins for the first demonstration
tSep = round(linspace(0, nbData, model.nbStates+1));

%Compute statistics for each bin
for i=1:model.nbStates
	id=[];
	for n=1:nbSamples
		id = [id (n-1)*nbData+[tSep(i)+1:tSep(i+1)]];
	end
	model.Priors(i) = length(id);
	model.Mu(:,i) = mean(Data(:,id),2);
	model.Sigma(:,:,i) = cov(Data(:,id)') + eye(size(Data,1)) * model.params_diagRegFact;
end
model.Priors = model.Priors / sum(model.Priors);

end

function [expData, expSigma, H] = GMR(model, DataIn, in, out)
% Gaussian mixture regression (GMR)
%
% If this code is useful for your research, please cite the related publication:
%
% @incollection{Calinon19MM,
%   author="Calinon, S.",
%   title="Mixture Models for the Analysis, Edition, and Synthesis of Continuous Time Series",
%   booktitle="Mixture Models and Applications",
%   publisher="Springer",
%   editor="Bouguila, N. and Fan, W.", 
%   year="2019"
% }
%
% Copyright (c) 2019 Idiap Research Institute, http://idiap.ch/
% Written by Sylvain Calinon, http://calinon.ch/
% 
% This file is part of PbDlib, http://www.idiap.ch/software/pbdlib/
% 
% PbDlib is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License version 3 as
% published by the Free Software Foundation.
% 
% PbDlib is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with PbDlib. If not, see <http://www.gnu.org/licenses/>.


nbData = size(DataIn,2);
nbVarOut = length(out);

if ~isfield(model,'params_diagRegFact')
	model.params_diagRegFact = 1E-8; %Regularization term is optional
end

MuTmp = zeros(nbVarOut, model.nbStates);
expData = zeros(nbVarOut, nbData);
expSigma = zeros(nbVarOut, nbVarOut, nbData);
for t=1:nbData
	%Compute activation weight
	for i=1:model.nbStates
		H(i,t) = model.Priors(i) .* gaussPDF(DataIn(:,t), model.Mu(in,i), model.Sigma(in,in,i));
	end
	H(:,t) = H(:,t) ./ sum(H(:,t)+realmin);
	%Compute conditional means
	for i=1:model.nbStates
		MuTmp(:,i) = model.Mu(out,i) + model.Sigma(out,in,i) / model.Sigma(in,in,i) * (DataIn(:,t)-model.Mu(in,i));
		expData(:,t) = expData(:,t) + H(i,t) .* MuTmp(:,i);
	end
	%Compute conditional covariances
	for i=1:model.nbStates
		SigmaTmp = model.Sigma(out,out,i) - model.Sigma(out,in,i) / model.Sigma(in,in,i) * model.Sigma(in,out,i);
		expSigma(:,:,t) = expSigma(:,:,t) + H(i,t) .* (SigmaTmp + MuTmp(:,i) * MuTmp(:,i)');
	end
	expSigma(:,:,t) = expSigma(:,:,t) - expData(:,t) * expData(:,t)' + eye(nbVarOut) * model.params_diagRegFact; 
end
end

function prob = gaussPDF(Data, Mu, Sigma)
% Likelihood of datapoint(s) to be generated by a Gaussian parameterized by center and covariance.
% Inputs -----------------------------------------------------------------
%   o Data:  D x N array representing N datapoints of D dimensions.
%   o Mu:    D x 1 vector representing the center of the Gaussian.
%   o Sigma: D x D array representing the covariance matrix of the Gaussian.
% Output -----------------------------------------------------------------
%   o prob:  1 x N vector representing the likelihood of the N datapoints.
%
% Writing code takes time. Polishing it and making it available to others takes longer! 
% If some parts of the code were useful for your research of for a better understanding 
% of the algorithms, please reward the authors by citing the related publications, 
% and consider making your own research available in this way.
%
% @article{Calinon16JIST,
%   author="Calinon, S.",
%   title="A Tutorial on Task-Parameterized Movement Learning and Retrieval",
%   journal="Intelligent Service Robotics",
%   publisher="Springer Berlin Heidelberg",
%   doi="10.1007/s11370-015-0187-9",
%   year="2016",
%   volume="9",
%   number="1",
%   pages="1--29"
% }
%
% Copyright (c) 2015 Idiap Research Institute, http://idiap.ch/
% Written by Sylvain Calinon, http://calinon.ch/
% 
% This file is part of PbDlib, http://www.idiap.ch/software/pbdlib/
% 
% PbDlib is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License version 3 as
% published by the Free Software Foundation.
% 
% PbDlib is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with PbDlib. If not, see <http://www.gnu.org/licenses/>.


[nbVar,nbData] = size(Data);
Data = Data - repmat(Mu,1,nbData);
prob = sum((Sigma\Data).*Data,1);
prob = exp(-0.5*prob) / sqrt((2*pi)^nbVar * abs(det(Sigma)) + realmin);

end