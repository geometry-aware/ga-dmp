classdef riemanDMP

    properties
        % settings
        DOF % degrees of freedom (number of DMPs)
        N % number of kernel functions per DMP
        alpha_z % DMP gain alpha
        beta_z % DMP gain beta
        alpha_Goal
        alpha_PhStop
        alpha_x
        dt % sample time
        diag
        tau

        % DMP learning variables
        f % shape function
        w % DMP weights
        c % centers of kernel functions
        sigma2
        P % regression variable P
        r % amplitude parameter
        g % DMP goal variable
        gnew % DMP goal variable
        psi

        % DMP state variables
        y, z, zd

        % DMP phase
        x
        % Manifold
        M
    end

    methods
        function obj = riemanDMP(m,DOF, N, alpha_z,alpha_x, tau, dt, y0, g)
            % create the parser
            ip = inputParser();
            % required inputs
            addRequired(ip,'M');
            obj.M = m;
            % optional inputs
            %addParameter(p, 'N', 1, @(x)validateattributes(x,...
            %    {'numeric'}, {'scalar','positive'}, 'nonempty'));
            obj.N = N;
            %addParameter(p, 'FrameSize', 1, @(x)validateattributes(x,...
            %    {'numeric'}, {'scalar','positive'}, 'FrameSize'));
            obj.alpha_z = alpha_z;
            %addParameter(p, 'FrameSize', 1, @(x)validateattributes(x,...
            %    {'numeric'}, {'scalar','positive'}, 'FrameSize'));
            obj.alpha_x = alpha_x;
            %addParameter(p, 'FrameSize', 1, @(x)validateattributes(x,...
            %    {'numeric'}, {'scalar','positive'}, 'FrameSize'));
            obj.alpha_Goal = 20;
            %addParameter(p, 'FrameSize', 1, @(x)validateattributes(x,...
            %    {'numeric'}, {'scalar','positive'}, 'FrameSize'));
            obj.tau = tau;
            %addParameter(p, 'dt', [], @(x)validateattributes(x,...
            %    {'numeric'}, {'nonempty'}));
            obj.dt = dt;
            %addParameter(p, 'FrameSize', 1, @(x)validateattributes(x,...
            %    {'numeric'}, {'scalar','positive'}, 'FrameSize'));
            obj.alpha_PhStop = 1;
            %addParameter(p, 'FrameSize', 1, @(x)validateattributes(x,...
            %    {'numeric'}, {'scalar','positive'}, 'FrameSize'));
            obj.g = g;
            %addParameter(p, 'FrameSize', 1, @(x)validateattributes(x,...
            %    {'numeric'}, {'scalar','positive'}, 'FrameSize'));
            obj.y = y0;
            %addParameter(p, 'FrameSize', 1, @(x)validateattributes(x,...
            %    {'numeric'}, {'scalar','positive'}, 'FrameSize'));
            obj.z = zeros(obj.DOF,1);
            %addParameter(p, 'FrameSize', 1, @(x)validateattributes(x,...
            %    {'numeric'}, {'scalar','positive'}, 'FrameSize'));
            obj.zd = zeros(obj.DOF,1);

            obj.DOF = sum(m.dim());
            obj.beta_z = alpha_z/4;
            obj.dt = dt;
            obj.tau = tau;
            obj.diag = obj.M.log(y0,g);%obj.diag(~obj.diag)=1;
            % DMP learning variables
            obj.f = zeros(obj.DOF,1);
            obj.w = zeros(obj.DOF,obj.N);
            obj.psi = zeros(obj.N,1);
            obj.P = ones(obj.DOF,obj.N);
            obj.gnew = g;
            %obj.g(1) = 1;
            % DMP state variables
            %obj.y(1) = 1;
            % define centers of kernel functions
            obj.c = exp(-obj.alpha_x * linspace(0, 1, obj.N));
            obj.sigma2 = (diff(obj.c)*0.75).^2;
            obj.sigma2 = [obj.sigma2, obj.sigma2(end)];
            % Phase
            obj.x = 1;
        end

        function  [y, dy, tau, phi] = get_state(obj)
            % GET STATE
            dy = obj.z ./ obj.tau;
            y = obj.y;
            tau = obj.tau;
            phi = obj.phi;
        end
        function w = get_w(obj, DOF)
            % GET DMP WEIGHTS
            w = obj.w(DOF,:);
        end
        function obj = learn(obj, y, dy, ddy)
            if ~iscell(y)
                %assert(isvector(y),'The data must be in a vector-cell');
                error('Trajectoy Y is not a vector of cells');
            end
            if (nargin<5)||(isempty(method))
                method = 'logeuclid';
            end

            switch method
                case 'LS'
                    obj = leastSquateRegression(obj,y,dy,ddy);
                case 'LWR'
                    obj = locallyWeightedRegression(obj,y,dy,ddy);
                otherwise
                    obj = leastSquateRegression(obj,y,dy,ddy);
            end
        end

        function S = integration(obj,S)

            S.psi = exp(-(S.x-obj.c).^2./(2*obj.sigma2));
            % the weighted sum of the locally weighted regression models
            if S.x >= exp(-obj.alpha_x)
                %psi = exp(-(S.x-DMP.c).^2./(2*DMP.sigma2));
                S.f = sum( sum((obj.w(:,:)*S.x).*S.psi,2)/sum(S.psi),3);
            else
                S.f = zeros(obj.DOF,1);
            end
            % integration of angular velocity
            log_data = obj.M.log(S.y,S.g);
            S.zd = obj.alpha_z * (obj.beta_z * log_data - S.z) + S.f .* obj.diag;
            % temporal scaling
            S.zd = S.zd / obj.tau;
            S.z = S.z + S.zd * obj.dt;

            if isstruct(S.y)
                z_ = constructVAstruct(obj,S.y, S.z/obj.tau);
            else
                z_ = S.z/obj.tau;
            end

            S.y = obj.M.exp(S.y,z_, obj.dt);

            % Goal Switching: smooth transition to the new goal.
            do = obj.alpha_Goal * obj.M.log(S.g,S.gnew);
            if isstruct(S.y)
                do_ = constructVAstruct(obj,S.y, do);
            else
                do_ = do;
            end
            if norm(do) > 1.0e-12
                S.g = obj.M.exp(S.g,do_,obj.dt);
            end
            % phase variable
            dx = -obj.alpha_x * S.x / (1 + obj.alpha_PhStop * obj.M.dist(S.y, S.y));
            dx = dx / obj.tau;
            S.x = S.x + dx * obj.dt;
        end
    end
end

function V = constructVAstruct(obj,y, z)
elems = fieldnames(y);
nelems = numel(elems);
V = struct();
dims = obj.M.dim();
ids = obj.M.id();
for i = 1 : nelems
    if i==1,    r = 0;  else,   r = sum(dims(i-1)); end
    switch ids{i}%elems{i}
        case 'SO'%'R'
            %V.(elems{i}) = z(i+(i-1)*2:3+(i-1)*3);
            V.(elems{i}) = z(r+1:sum(dims(1:i)));
        case 'R'%'t'
            V.(elems{i}) = z(r+1:sum(dims(1:i)));
        case 'S3'%'t'
            V.(elems{i}) = z(r+1:sum(dims(1:i)));
        case 'SPD'%'t'
            V.(elems{i}) = z(r+1:sum(dims(1:i)));
        otherwise
            disp('other value')
    end
end
end

function obj = leastSquateRegression(obj,y,dy,ddy)
n = length(y);
obj.f = zeros(n,obj.DOF);
A = zeros(n, obj.N);
obj.diag = obj.M.log(y{1},y{end});
d = 1./obj.diag';
d(isinf(d)|isnan(d)) = 1;
x_ = 1;
for i = 1:n
    obj.psi = exp(-(x_-obj.c).^2./(2*obj.sigma2));
    x_ = x_ - obj.alpha_x*x_*obj.dt/obj.tau;

    obj.f(i,:) = d.*(obj.tau^2*ddy(:,i) + ...
        obj.alpha_z*obj.tau*dy(:,i) - ...
        obj.alpha_z*obj.beta_z*obj.M.log(y{i},obj.g))';

    A(i,:) = x_*obj.psi/sum(obj.psi);
end
obj.w = (A \ obj.f)';
end

function obj = locallyWeightedRegression(obj,y,dy,ddy)
n = length(y);
obj.diag = obj.M.log(y{1},y{end});
d = 1./obj.diag';
d(isinf(d)|isnan(d)) = 1;
obj.f = zeros(n,obj.DOF);
A = zeros(n, obj.N);
x_ = 1;
P_ = zeros(obj.N,obj.N,obj.M.dim());
for i = 1:obj.M.dim()
    P_(:,:,i) = eye(obj.N)*1000;    % initial large covariance
end
for i = 1:n
    obj.psi = exp(-(x_-obj.c).^2./(2*obj.sigma2));
    x_ = x_ - obj.alpha_x*x_*obj.dt/obj.tau;

    obj.f(i,:) = d.*(obj.tau^2*ddy(:,i) + ...
        obj.alpha_z*obj.tau*dy(:,i) - ...
        obj.alpha_z*obj.beta_z*obj.M.log(y{i},obj.g))';

    for k = 1:obj.M.dim()
        %% recursive regression step
        xx = obj.psi'*x_/sum(obj.psi);
        % calculation of all weights over entire interval x
        Pk = P_(:,:,k);
        P_(:,:,k) = (Pk-(Pk*xx*xx'*Pk)/(1+xx'*Pk*xx));
        e = obj.f(i,k) - xx'*obj.w(k,:)';
        obj.w(k,:) = obj.w(k,:) + (e*P_(:,:,k)*xx)';
    end
    %A(i,:) = x_*obj.psi/sum(obj.psi);
end
end