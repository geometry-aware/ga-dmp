clear all
close all
clc

%% Generate covariance data from handwriting data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
LASA = 1;
PLOT = 0;
if LASA
    % S. M. Khansari-Zadeh and A. Billard, "Learning Stable Non-Linear
    % Dynamical Systems with Gaussian Mixture Models", IEEE Transaction on
    % Robotics, 2011.
    p_path = './data/LASA_hand_writing_dataset/DataSet/';
    names = {'Angle','BendedLine','CShape','GShape', 'heee','JShape',...
        'Khamesh','LShape','NShape','PShape','RShape','Saeghe','Sharpc',...
        'Sine','Snake','Spoon','Sshape','Trapezoid','Worm','WShape',...
        'Zshape','JShape_2', 'Leaf_1', 'Leaf_2', 'DoubleBendedLine',...
        'Line','Multi_Models_1','Multi_Models_2','Multi_Models_3',...
        'Multi_Models_4'};
    dt = 0.0025;
    scale = 100;
    nbSamples0 = 6;
else
    % Calinon S. Gaussians on Riemannian manifolds: Applications for robot
    % learning and adaptive control. IEEE Robotics & Automation Magazine.
    % 2020 Apr 6;27(2):33-45.
    p_path = 'data/2Dletters/';
    names = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N',...
        'O','P','Q','R','S','T','U','V','W','X','Y','Z'};
    dt = 0.01;
    scale = 50;
    nbSamples0 = 9;
end
spd_ref = eye(2)*100;
so3_ref = eye(3);
qut_ref = [1 0 0 0];
for l = 4%1:length(names)
    load([p_path names{l} '.mat']);
    nbData = length(demos{1}.pos);
    ts = [];
    for n=1:nbSamples0
        tmp.Data = spline(1:size(demos{n}.pos,2), demos{n}.pos, linspace(1,size(demos{n}.pos,2),nbData)); %Resampling
        wOut = [tmp.Data; tmp.Data(1,:)];
        %wOut = [demos{n}.pos; mean(demos{n}.pos)];
        quat = quat_exp(qut_ref',wOut/scale);
        rot = rot_exp(so3_ref,wOut/scale);
        spd = spd_exp(spd_ref,wOut);
        for i = 2:length(quat)
            if quat(:,i-1)'*quat(:,i)<0
                quat(:,i) = - quat(:,i);
            end
        end
        demos{n}.quat = quat;
        demos{n}.rot = rot;
        demos{n}.spd = spd;
        demos{n}.POS = wOut;
    end



    %% Save new data
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %eval([names{l},'.cov = covOut']);
    %eval([names{l},'.posOut = posOut']);
    %eval([names{l},'.qutOut = xhat']);
    eval([names{l},'.demos = demos'])

    save(['./data/dataset_MAN/' names{l} '_MAN.mat'],names{l})
end
return


%% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function y = quat_exp(q,w)
if nargin == 1
    q = [1 0 0 0]';
end
for i = 1:size(w,2)
    nrm_w = norm(w(:,i), 'fro');
    if nrm_w > 0
        y(:,i) = [cos(nrm_w); w(:,i)*sin(nrm_w)/nrm_w];
        y(:,i) = y(:,i)/norm(y(:,i));
        y(:,i) = quat_multiply(y(:,i),q);
    else
        y(:,i) = [1 0 0 0];
    end
end
end
function log_q = quat_log(q1,q2)
n = size(q2,2);
log_q = zeros(3,n);
for i = 1:n
    q2c = quat_conjugate(q2(:,i));
    q = quat_multiply(q1, q2c);

    tmp = norm(q);
    q = q/tmp;

    if norm(q(2:4)) > 1.0e-12
        log_q(:,i) = 2* acos(q(1)) * q(2:4)' / norm(q(2:4));
    else
        log_q(:,i) = [0; 0; 0];
    end
end
end

function q = quat_conjugate(q)
q(2:4) = -q(2:4);
end
function q = quat_multiply(q1, q2)
if isrow(q1),   q1 = q1';   end
if isrow(q2),   q2 = q2';   end
q(1) = q1(1) * q2(1) - q1(2:4)' * q2(2:4);
q(2:4) = q1(1) * q2(2:4) + q2(1) * q1(2:4) + ...
    [q1(3)*q2(4) - q1(4)*q2(3); ...
    q1(4)*q2(2) - q1(2)*q2(4); ...
    q1(2)*q2(3) - q1(3)*q2(2)];
end

function Y = spd_exp(X, V, dt)
symm = @(X) .5*(X+X');
if nargin < 3
    dt = 1.0;
end
[~,c] = size(V);
S = vec2symMat(V);
% The symm() and real() calls are mathematically not necessary but
% are numerically necessary.
for i = 1:c
    Y(:,:,i) = symm(X*real(expm(X\(dt*S(:,:,i)))));
end
end
function RR = rot_exp(R,w, t)
if nargin < 3
    t = 1;
end
dR = vecMat(t*w);
RR = dR * R;
end
function R = vecMat(omega)
%-------------------------------------------------------------------------
% Return the rotation matrix equal to exp(omega)

    rn = norm (omega);
    if rn > 1.0e-12
        r = omega/rn;
        s = sin(rn); c = 1-cos(rn);

        srx = s*r(1); sry = s*r(2); srz = s*r(3);

        crx = c*r(1); cry = c*r(2); crz = c*r(3);

        crxrx = crx*r(1); cryry = cry*r(2); crzrz = crz*r(3);
        crxry = crx*r(2); cryrz = cry*r(3); crxrz = crx*r(3);

        R = [1-cryry-crzrz -srz+crxry sry+crxrz;...
            srz+crxry 1-crxrx-crzrz -srx+cryrz;...
            -sry+crxrz srx+cryrz 1-crxrx-cryry];
    else
        R = eye(3);
    end
end