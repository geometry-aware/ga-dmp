function fig = plot_spd_cone(r,varargin)

% setup input parser
p = inputParser;

addParameter(p, 'Parent', [], ...
    @(x)validateattributes(x, {'matlab.graphics.axis.Axes'}, {'nonempty', 'scalar'}, 'plotTransforms', 'Parent'));
addParameter(p, 'color', [], @(x)validateattributes(x, {'string', 'char','numeric'}, {'nonempty'}, 'color'));
addParameter(p, 'LineStyle', [], @(x)validateattributes(x, {'string', 'char'}, {'nonempty'}, 'LineStyle'));
addParameter(p, 'FaceColor', [], @(x)validateattributes(x, {'string', 'char','numeric'}, {'nonempty'}, 'FaceColor'));
addParameter(p, 'FaceAlpha', [], @(x)validateattributes(x, {'string', 'char','numeric'}, {'nonempty'}, 'FaceAlpha'));
addParameter(p, 'View', [], @(x)validateattributes(x, {'string', 'char', 'numeric'}, {'nonempty'}, 'plotTransforms', 'View'));

% parse inputs
parse(p, varargin{:})

% prepare geodesic color
if ~isempty(p.Results.FaceAlpha)
    faceAlpha = p.Results.FaceAlpha;
else
    faceAlpha = 1;
end
% prepare geodesic color
if ~isempty(p.Results.color)
    color = p.Results.color;
else
    color = [.8 .8 .8];
end
% prepare face color
if ~isempty(p.Results.FaceColor)
    faceColor = p.Results.FaceColor;
else
    faceColor = [.85 .85 .85];
end
% prepare geodesic color
if ~isempty(p.Results.LineStyle)
    LineStyle = p.Results.LineStyle;
else
    LineStyle = '-';
end


if ~nargin
    r = 20;  % or whatever
end
%% Plot SPD cone
%% Plot SPD cone
% prepare ax for plotting
if isempty(p.Results.Parent)
    fig = newplot; %figure('position',[280 85 589 441],'color',[1 1 1]);
    set(gcf, 'color', [1 1 1])
    axis equal
    grid off
    %axis off
    parentAx = fig;%.CurrentAxes;%
    hold on;
else
    parentAx = p.Results.Parent;
    hold on;
end
% Plot the SPD convex cone
phi = 0:0.1:2*pi+0.1;
x = [zeros(size(phi)); r.*ones(size(phi))];
y = [zeros(size(phi));r.*sin(phi)];
z = [zeros(size(phi));r/sqrt(2).*cos(phi)];

h = mesh(x,y,z,'linestyle','none','FaceColor',faceColor,'facealpha',faceAlpha);
direction = cross([1 0 0],[1/sqrt(2),1/sqrt(2),0]);
rotate(h,direction,45,[0,0,0])

h = plot3(x(2,:),y(2,:),z(2,:),'linewidth',2,'color',color);
rotate(h,direction,45,[0,0,0])
% h = plot3(x(:,63),y(:,63),z(:,63),'linewidth',2,'color',[0 0 0]);
% rotate(h,direction,45,[0,0,0])
% h = plot3(x(:,40),y(:,40),z(:,40),'linewidth',2,'color',[0 0 0]);
% rotate(h,direction,45,[0,0,0])
% Settings
set(gca,'XTick',[],'YTick',[],'ZTick',[]);
axis off
% xlabel('\alpha'); ylabel('\gamma'); zlabel('\beta');
if ~isempty(p.Results.View)
    viewAzEl = validateViewInput(p.Results.View);
    view(parentAx, viewAzEl(1), viewAzEl(2));
else
    view(70,12);
end

end

function selectedView = validateViewInput(inputViewValue)
%validateViewInput Validate the input the the "View" Name/Value pair
%   The earlier check verifies that the user-specified input is nonempty,
%   but the default value (no input provided) is empty. This function
%   converts the input options to a 2-element vector [AZ EL] containing the
%   azimuth and elevation angles that are passed to the view command
%   downstream.
%   Copyright 2018-2019 The MathWorks, Inc.

if isnumeric(inputViewValue) && ~isempty(inputViewValue)
    % If the input is numeric, extract azimuth and elevation angles
    validateattributes(inputViewValue, {'numeric'}, {'vector', 'numel', 2, 'finite'}, 'plotTransforms', 'View');
    selectedView = inputViewValue;
else
    % If the input is a string, it should specify 2D or 3D. Convert
    % these two azimuth and elevation angles
    viewStr = validatestring(inputViewValue, {'2D', '3D'}, 'plotTransforms', 'View');
    if strcmp(viewStr, '2D')
        [az, el] = view(2);
    else
        [az, el] = view(3);
    end
    selectedView = [az el];
end

end