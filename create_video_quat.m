demo_quat_02

quat_cart_fig=figure('position',[460 -1076 919 985]);hold on; view([-13.7935   62.1800]);box on;
quat_cart_fig.CurrentAxes.XAxis.Limits = [-20.0733   19.0263];
quat_cart_fig.CurrentAxes.YAxis.Limits = [-20.7379   20.1453];
quat_cart_fig.CurrentAxes.ZAxis.Limits = [-16.1492   17.9119];
axis equal; axis tight;
set(gca, 'FontSize', 15);
xlabel('$x$', 'FontSize', 25, 'interpreter','latex');
ylabel('$y$', 'FontSize', 25, 'interpreter','latex');
zlabel('$z$', 'FontSize', 25, 'interpreter','latex');
quat_cart_fig.CurrentAxes.TickLabelInterpreter = 'latex';

fps = 30;
writerObj1 = VideoWriter('quat.avi'); % Name it.
writerObj1.FrameRate = fps;	% How many frames per second.
writerObj1.Quality  = 100;	% Video Quality.
open(writerObj1);

plot3(pos(:,1), pos(:,2), pos(:,3),'linewidth',1.5,'color','k');
for i=round(linspace(1,n,n/4))
    R = quat2rotm( Sorig(:,i)' );
    HP1 = quiver3( pos(i,1), pos(i,2), pos(i,3), R(1,1), R(2,1), R(3,1), 5, 'linewidth', 1.5, 'color', [.7 .7 .7]);
    HP1 = quiver3( pos(i,1), pos(i,2), pos(i,3), R(1,2), R(2,2), R(3,2), 5, 'linewidth', 1.5, 'color', [.7 .7 .7]);
    HP1 = quiver3( pos(i,1), pos(i,2), pos(i,3), R(1,3), R(2,3), R(3,3), 5, 'linewidth', 1.5, 'color', [.7 .7 .7]);
end
for i=round(linspace(1,n,n/6))
    R = quat2rotm( Q(i,:) );
    HP1(2) = quiver3( pos(i,1), pos(i,2), pos(i,3), R(1,1), R(2,1), R(3,1), 5, 'linewidth', 3, 'color', mycolors.r);
    HP1(3) = quiver3( pos(i,1), pos(i,2), pos(i,3), R(1,2), R(2,2), R(3,2), 5, 'linewidth', 3, 'color', mycolors.g);
    HP1(4) = quiver3( pos(i,1), pos(i,2), pos(i,3), R(1,3), R(2,3), R(3,3), 5, 'linewidth', 3, 'color', mycolors.b);
    frame = getframe(gcf);
    writeVideo(writerObj1, frame);
end
close(writerObj1);
