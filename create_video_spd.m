demo_SPD_02

spd_cart_fig=figure('position',[469 -1075 1211 984]);hold on; box on;
spd_cart_fig.CurrentAxes.XAxis.Limits = [-25.1997   19.4085];
spd_cart_fig.CurrentAxes.YAxis.Limits = [-24.1197   18.8432];
axis equal; axis tight;
set(gca, 'FontSize', 15);
xlabel('$x$', 'FontSize', 25, 'interpreter','latex');
ylabel('$y$', 'FontSize', 25, 'interpreter','latex');
spd_cart_fig.CurrentAxes.TickLabelInterpreter = 'latex';

fps = 30;
writerObj1 = VideoWriter('spd.avi'); % Name it.
writerObj1.FrameRate = fps;	% How many frames per second.
writerObj1.Quality  = 100;	% Video Quality.
open(writerObj1);

for i=round(linspace(1,n,n/10))
    [HP1, ~] = plot2x2elipsoids(pos(i,:)', 2*Y{i,1}, [.7 .7 .7], .4,0.1); % Scaled matrix!
end
plot(pos(:,1), pos(:,2),'linewidth',1.5,'color','k');
for i=round(linspace(1,n,50))
    [HP2, ~] = plot2x2elipsoids(pos(i,:)', 2*XX(:,:,i), [0 1 0], .4); % Scaled matrix!
    frame = getframe(gcf);
    writeVideo(writerObj1, frame);
end
plot(pos(:,1), pos(:,2),'linewidth',1.5,'color','k');
close(writerObj1);
