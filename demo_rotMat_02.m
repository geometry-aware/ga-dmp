clear all
close all

clc
 
addpath(genpath('factories'));
addpath(genpath('tools'));

%% Load data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rawData = load('data/dataset_MAN/GShape_MAN.mat');
variables=fields(rawData);
Sorig = rawData.(variables{1}).demos{1}.rotm;
pos = rawData.(variables{1}).demos{1}.tsPos';
n = length(Sorig);
Qorig = zeros(n, 9);
for i=1:n
    Qorig(i,:) = reshape(Sorig(:,:,i), 1, 9);
end

myColors;

%% Fit S3 data using piecewise geodesic interpolating
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
M = so3Factory(3);
tau = 10;
[Y, dy, ddy, t] = interpolationRot(M, Sorig, tau);

%% SET DMP parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dt=t(2)-t(1);
 
N = 50;
alpha_qGoal = 0.5;
alpha_z = 48/3;
alpha_x = 2;
alpha_pPos = 400;
alpha_pOri = 4;
DOF = M.dim();
y0 = Y{1};
yg = Y{end};
tau = 10;

%% Initialize DMP object
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp(['---DMP for ' M.name() '---']);
DMP = riemanDMP(M, DOF, N, alpha_z,alpha_x,tau,dt,y0,yg);

%% DMP estimate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
DMP = learn(DMP, Y, dy, ddy);

%% Initial states
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
duration = 0;

S.y = DMP.y;
S.g = DMP.g;
S.gnew = DMP.g;
goalSind = 2*n;
duration = 0;

goal_switching = 0;
if goal_switching
    newGoal = double(UnitQuaternion([0.7 0.35 0.5 0.3]));
    goalSind = 500;
end

XX = zeros(3,3,round((DMP.tau-duration)/DMP.dt)+1);
XX(:,:,1) = S.y;

Q = zeros(round((DMP.tau-duration)/DMP.dt)+1, 9);
Q(1,:) = reshape(S.y, 1, 9);

dQ = zeros(round((DMP.tau-duration)/DMP.dt), 3);
S.z =  zeros(3,1);%dy(:,1); 
dQ(1,:) = S.z;

ddQ = zeros(round((DMP.tau-duration)/DMP.dt), 3);
S.zd = zeros(3,1);%ddy(:,1);
ddQ(1,:) = S.zd;

x = zeros(round((DMP.tau-duration)/DMP.dt)+1, 1);
S.x = 1;
x(1) = 1;

time = zeros(round((DMP.tau-duration)/DMP.dt), 1);

FX = zeros(round((DMP.tau-duration)/DMP.dt)+1, M.dim());
PSI = zeros(round((DMP.tau-duration)/DMP.dt)+1,N);

i = 2;

while S.x > exp(-DMP.alpha_x * (DMP.tau - duration + DMP.dt) / DMP.tau)
    if goal_switching && i == goalSind
        S.gnew = gnew;
    end
    S = integration(DMP, S);
    x1(i) = S.x;
    XX(:,:,i) = S.y;
    Q(i,:) = reshape(S.y, 1, 9);
    dQ(i,:) = S.z'/DMP.tau;             % angular velocities
    ddQ(i,:) = S.zd'/DMP.tau;            % angular acc
    FX(i,:) = S.f';
    PSI(i,:) = S.psi;
    time(i) = time(i-1) + DMP.dt;
    i = i + 1;
end
return
%% Plotting PSI
rot_psi_fig = figure('position',[729 -609 241 119]);hold on;box on;
plot(time,PSI,'linewidth',1.5)
for ll = 1:length(PSI)
    wPSI(ll,:) = PSI(ll,:)'.*DMP.w(1,:)';
end
axis tight
ylabel('$\\Psi_i(x)$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
print(rot_psi_fig,'-dsvg','-r600','results/rot_psi_fig');
print(rot_psi_fig,'-dpdf','-r600','results/rot_psi_fig');
print(rot_psi_fig,'-dpng','-r600','results/rot_psi_fig');

%% Plotting wPSI
rot_wpsi_fig = figure('position',[729 -609 241 119]);hold on;box on;
plot(time,wPSI,'linewidth',1.5)
axis tight
ylabel('$\w_i\Psi_i(x)$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
print(rot_wpsi_fig,'-dsvg','-r600','results/rot_wpsi_fig');
print(rot_wpsi_fig,'-dpdf','-r600','results/rot_wpsi_fig');
print(rot_wpsi_fig,'-dpng','-r600','results/rot_wpsi_fig');

%% Plotting SPD upertriangle elements
rot_elem_fig = figure('position',[729 -609 241 119]);hold on;box on;
p2 = plot(time,Q,'linewidth',2);
p1 = plot(time,Qorig,'k-.','linewidth',2);
[leg1, leg2] = legend([p1(1) p2(1)],{'$\bcalR^{demo}$','colored $\bcalR^{dmp}$'},'color','none')
%legend([p1(1) p2(1)],{'$\bm{R}^{demo}$','colors $\bm{R}^{dmp}$'},'color','none')
ylabel('$\bcalR$ elements','Fontsize',10)
xlabel('Time','Fontsize',10);
print(rot_elem_fig,'-dsvg','-r600','results/rot_elem_fig');
print(rot_elem_fig,'-dpdf','-r600','results/rot_elem_fig');
print(rot_elem_fig,'-dpng','-r600','results/rot_elem_fig');

%% Plot distances
dist_R = zeros(round((DMP.tau-duration)/DMP.dt)+1, 1);
rot_dist_fig=figure('position',[729 -609 241 119]);hold on;box on;
for i=1:n
    dist_R(i) = M.dist(XX(:,:,i),Y{i});
end
plot(time,dist_R,'color',mycolors.b,'linewidth',1.5);%plot(d_KL);
ylabel('$error$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
set(gca,'Fontsize', 10);
print(rot_dist_fig,'-dsvg','-r600','results/rot_dist_fig');
print(rot_dist_fig,'-dpdf','-r600','results/rot_dist_fig');
print(rot_dist_fig,'-dpng','-r600','results/rot_dist_fig');

%% Plot stiffness over Cartesian
rot_cart_fig=figure('position',[758 -572 221 170]);hold on; view([-13.7935   62.1800]);box on;
plot3(pos(:,1), pos(:,2), pos(:,3),'linewidth',1.5,'color','k');
for i=round(linspace(1,n,n/4))
    RR = Y{i};
    HP1 = quiver3( pos(i,1), pos(i,2), pos(i,3), RR(1,1), RR(2,1), RR(3,1), 5, 'linewidth', 1.5, 'color', [.7 .7 .7]);
    HP1 = quiver3( pos(i,1), pos(i,2), pos(i,3), RR(1,2), RR(2,2), RR(3,2), 5, 'linewidth', 1.5, 'color', [.7 .7 .7]);
    HP1 = quiver3( pos(i,1), pos(i,2), pos(i,3), RR(1,3), RR(2,3), RR(3,3), 5, 'linewidth', 1.5, 'color', [.7 .7 .7]);
end
for i=round(linspace(1,n,n/6))
    RR = XX(:,:,i);
    HP1(2) = quiver3( pos(i,1), pos(i,2), pos(i,3), RR(1,1), RR(2,1), RR(3,1), 5, 'linewidth', 3, 'color', mycolors.r);
    HP1(3) = quiver3( pos(i,1), pos(i,2), pos(i,3), RR(1,2), RR(2,2), RR(3,2), 5, 'linewidth', 3, 'color', mycolors.g);
    HP1(4) = quiver3( pos(i,1), pos(i,2), pos(i,3), RR(1,3), RR(2,3), RR(3,3), 5, 'linewidth', 3, 'color', mycolors.b);
end
axis equal; axis tight;
HP = [HP1(1) HP2(1)];
legText{1} = '$\bm{C}$';
legText{2} = '$\bm{\hat{C}}$';
if goal_switching
    [HP3, ~] = plot2x2elipsoids(pos(1:2,goalSind), .1*XX(:,:,goalSind),...
        [0 0 1], .4);
    legText{3} = 'Switching starts';
    [HP4, ~] = plot2x2elipsoids(pos(1:2,end), .1*S.gnew,...
        [1 0 0], .4);
    HP = [HP HP3(1) HP4(1)];
    legText{4} = 'New goal';
end
ylabel('$y$', 'Fontsize', 10);
ylabel('$y$', 'Fontsize', 10);
xlabel('$x$', 'Fontsize', 10);
set(gca,'TickLabelInterpreter','latex', 'Fontsize', 10);
print(rot_cart_fig,'-dsvg','-r600','results/rot_cart_fig');
print(rot_cart_fig,'-dpdf','-r600','results/rot_cart_fig');
print(rot_cart_fig,'-dpng','-r600','results/rot_cart_fig');
h = legend(HP, legText, 'Location','northwest');
set(h, 'Fontsize', 10,'color','none');
clear HP HP1 HP2 HP3 HP4 legText

%% Plot Rotation around [0,0,0]'
rot_0_fig=figure('position',[758 -572 221 170]);hold on; view([127.8133   11.0665]);box on;
for i=round(linspace(1,n,n/4))
    RR = XX(:,:,i);    err = (t(n-i+1)*90/n + 0.1);
    rr = mycolors.r * err;    gg = mycolors.g * err;    bb = mycolors.b * err;
    HP1(1) = quiver3( pos(i,1)*0, pos(i,2)*0, pos(i,3)*0, RR(1,1), RR(2,1), RR(3,1), 1, 'linewidth', 3, 'color', rr);
    HP1(2) = quiver3( pos(i,1)*0, pos(i,2)*0, pos(i,3)*0, RR(1,2), RR(2,2), RR(3,2), 1, 'linewidth', 3, 'color', gg);
    HP1(3) = quiver3( pos(i,1)*0, pos(i,2)*0, pos(i,3)*0, RR(1,3), RR(2,3), RR(3,3), 1, 'linewidth', 3, 'color', bb);
end
axis equal; axis tight;
HP = [HP1(1) HP2(1)];
legText{1} = '$\bm{C}$';
legText{2} = '$\bm{\hat{C}}$';
if goal_switching
    [HP3, ~] = plot2x2elipsoids(pos(1:2,goalSind), .1*XX(:,:,goalSind),...
        [0 0 1], .4);
    legText{3} = 'Switching starts';
    [HP4, ~] = plot2x2elipsoids(pos(1:2,end), .1*S.gnew,...
        [1 0 0], .4);
    HP = [HP HP3(1) HP4(1)];
    legText{4} = 'New goal';
end
ylabel('$y$', 'Fontsize', 10);
xlabel('$x$', 'Fontsize', 10);
set(gca,'TickLabelInterpreter','latex', 'Fontsize', 10);
print(rot_0_fig,'-dsvg','-r600','results/rot_0_fig');
print(rot_0_fig,'-dpdf','-r600','results/rot_0_fig');
print(rot_0_fig,'-dpng','-r600','results/rot_0_fig');
h = legend(HP, legText, 'Location','northwest');
set(h, 'Fontsize', 10,'color','none');
clear HP HP1 HP2 HP3 HP4 legText

%% Plot Velocity
rot_Vel_fig=figure('position',[729 -609 241 119]);hold on;box on;
p2 = plot(time, dQ(:,1), 'color', mycolors.cw,'linewidth',2); %K_11
p3 = plot(time, dQ(:,3), 'color', mycolors.cy,'linewidth',2); %K_12
p4 = plot(time, dQ(:,2), 'color', mycolors.cg,'linewidth',2); %K_22
p1 = plot(time, dy','k-.','linewidth',2);
h = legend([p1(3) p2],'$\bcalW^{demo}$','colored $\bcalW^{dmp}$', 'color','none');
%h = legend([p1(3) p2 p3 p4],'$\bm{\omega}^{demo}$','$\omega_x$','$\omega_y$','$\omega_z$');
set(h, 'Fontsize', 10, 'Location','northwest', 'color','none');
ylabel('$\bcalW$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
axis tight;
set(gca, 'Fontsize', 9);
print(rot_Vel_fig,'-dsvg','-r600','results/rot_Vel_fig');
print(rot_Vel_fig,'-dpdf','-r600','results/rot_Vel_fig');
print(rot_Vel_fig,'-dpng','-r600','results/rot_Vel_fig');

%% Plot acceleration
rot_acc_fig=figure('position',[729 -609 241 119]);hold on;box on;
p1 = plot(time, ddy','k--','linewidth',2);
p2 = plot(time, ddQ(:,1), 'color', mycolors.cw,'linewidth',1.5); %K_11
p3 = plot(time, ddQ(:,3), 'color', mycolors.cy,'linewidth',1.5); %K_12
p4 = plot(time, ddQ(:,2), 'color', mycolors.cg,'linewidth',1.5); %K_22
h = legend([p1(3) p2 p3 p4],'$\bm{\dot{\omega}}_{demo}$','$\dot{\omega}_x$','$\dot{\omega}_y$','$\dot{\omega}_z$');
set(h, 'Fontsize', 10, 'Location','northwest', 'color','none');
ylabel('$\bm{\dot{\omega}}$', 'Fontsize', 10);
xlabel('Time', 'Fontsize', 10);
axis([0, time(end), -.6, .6]);
set(gca, 'Fontsize', 9);
print(rot_acc_fig,'-dsvg','-r600','results/rot_acc_fig');
print(rot_acc_fig,'-dpdf','-r600','results/rot_acc_fig');
print(rot_acc_fig,'-dpng','-r600','results/rot_acc_fig');

%% Plot S3 in sphere
rot_sphere_fig = figure;
pG = 'on';
plotOrientationOnUnitSphere(M,XX, 'Framesize', 0.01,...
    'View', [273.7835, 65.3944],'dt', 0.1, 'colormap', 'gray',...
    'nFrames',0, 'nBlochVector', 0,...
    'color',mycolors.cw, 'LineWidth',3)
plotOrientationOnUnitSphere(M,Sorig, 'Framesize', 0.5,...
    'View', [273.7835, 65.3944],'dt', 0.1, 'colormap', 'gray',...
    'nFrames',0, 'nBlochVector', 0,...
    'plotGoal',pG,'color','k', 'LineWidth',4,'LineStyle','-.')
legend('','$\bm{q}^{dmp}$','','$\bm{q}^{demo}$','$\G_q$','')
print(rot_sphere_fig,'-dsvg','-r600','results/rot_sphere_fig');
print(rot_sphere_fig,'-dpdf','-r600','results/rot_sphere_fig');
print(rot_sphere_fig,'-dpng','-r600','results/rot_sphere_fig');


myColors;
%quat_fig = figure(1); clf;
rot_fig=figure;%subplot(2,1,1);
hold on
% plot rotation matrix profile generated by DMP
plot(t1(1:i), Q(1:i,:),'linewidth',1.5);
plot(t1(1:i), Qorig(1:i,:),'--','linewidth',1.5);
ylabel('$\mathbf{R}$ elements','interpreter','latex','FontName','Times','Fontsize',12)
xlabel('Time [s]','interpreter','latex','FontName','Times','Fontsize',12);
rot_fig.CurrentAxes.TickLabelInterpreter = 'latex';
axis([0,DMP.tau-duration,-1.1,1.1]);

% plot angular velocity profile generated by DMP
omega_fig=figure;%subplot(2,1,2);
hold on;
plot(t1(1:i),W(1:i,:),'linewidth',1.5)
plot(t1(1:i),omega(:,1:i),'--','linewidth',1.5)
axis([0,DMP.tau-duration,min(min(W))-.1,max(max(W))+.1]);
ylabel('\boldmath${\omega}$ {[rad/s]}','interpreter','latex','FontName','Times','Fontsize',12)
xlabel('Time [s]','interpreter','latex','FontName','Times','Fontsize',12);
omega_fig.CurrentAxes.TickLabelInterpreter = 'latex';

% plot angular acceleration profile generated by DMP
domega_fig=figure; clf; hold on;
plot(t1(1:i),dW(1:i,:),'linewidth',1.5)
plot(t1(1:i),domega(:,1:i),'--','linewidth',1.5)
axis([0,DMP.tau-duration,min(min(dW))-.1,max(max(dW))+.1]);
ylabel('\boldmath${\dot{\omega}}$ {[rad/s$^2$]}','interpreter','latex','FontName','Times','Fontsize',12)
xlabel('Time [s]','interpreter','latex','FontName','Times','Fontsize',12);
domega_fig.CurrentAxes.TickLabelInterpreter = 'latex';

% plot psi
psi_fig=figure; clf; hold on;
plot(linspace(DMP.dt,length(PSI)*DMP.dt,length(PSI)),PSI,'linewidth',1.5)
ylabel('$\Psi_i(x)$', 'Fontsize', 10, 'Interpreter', 'Latex');
xlabel('$t$', 'Fontsize', 10, 'Interpreter', 'Latex');
for ll = 1:length(PSI)
    wPSI1(:,ll) = PSI(ll,:).*DMP.w(1,:);
    wPSI2(:,ll) = PSI(ll,:).*DMP.w(2,:);
    wPSI3(:,ll) = PSI(ll,:).*DMP.w(3,:);
end
wpsi_fig=figure; clf; hold on;
plot(linspace(DMP.dt,length(PSI)*DMP.dt,length(PSI)),wPSI1'+wPSI2'+wPSI3','linewidth',1.5)
ylabel('$w_i\Psi_i(x)$', 'Fontsize', 10, 'Interpreter', 'Latex');
xlabel('$t$', 'Fontsize', 10, 'Interpreter', 'Latex');

% plot phase
x_fig=figure; clf; hold on;
plot(linspace(DMP.dt,length(x1)*DMP.dt,length(x1)),x1,'linewidth',1.5)
xlabel('$t$', 'Fontsize', 10, 'Interpreter', 'Latex');



return

