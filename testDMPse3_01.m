
clear all
close all

clc
addpath(genpath('factories'));
addpath(genpath('tools'));
addpath(genpath('quaternionFunctions'));

M = seFactory(3);

tau = 10;
dt = 0.01;
N = round(tau / dt) + 1;
t = linspace(0, tau, N);

%% LOAD Data or Generate them
% Generate vector data
v1 = [5 3 0]';  v2 = [3 4 5]';
for j = 1:3
    a = minimum_jerk_spline(v1(j), 0, 0, v2(j), 0, 0, tau);
    for i = 1:N
        v(j,i) = minimum_jerk(t(i), a);
    end
    dv(j,:) = gradient(v(j,:), t);
end
for j = 1:3
    ddv(j,:) = gradient(dv(j,:), t);
end
for i = 1:N
    V{i,1} = v(:,i);
end
% Generate orientation data
q1.s = 0.3717;      q1.v = [-0.4993; -0.6162; 0.4825];
tmp = quat_norm(q1);
q1.s = q1.s / tmp;  q1.v = q1.v / tmp;
if q1.s < 0
    q1.s = -q1.s;   q1.v = -q1.v;
end

q2.s = 0.2471;      q2.v = [0.1797; 0.3182; -0.8974];
tmp = quat_norm(q2);
q2.s = q2.s / tmp;  q2.v = q2.v / tmp;
if q2.s < 0
    q2.s = -q2.s;   q2.v = -q2.v;
end
% be sure start and goal are at the same hemisphere.
if [q1.s q1.v']*[q2.s; q2.v] < 0
    q1.s = -q1.s;    q2.v = -q2.v;
end
[q, dq, ddq, t] = generate_orientation_data(q1, q2, tau, dt);
[R, dR, ddR] = convert_data(q, t);
for i=1:length(R)
    y{1,1,i}.R = R{i};
    y{1,1,i}.t = V{i};
    dy(:,i) = [dR(:,i); dv(:,i)];
    ddy(:,i) = [ddR(:,i); ddv(:,i)];
    Qorig(i,:) = reshape(R{i}, 1, 9);
end

%% SET DMP parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

M = seFactory(3);
dt=t(2)-t(1);

N = 20;
alpha_qGoal = 0.5;
alpha_z = 48;
alpha_x = 2;
alpha_pPos = 400;
alpha_pOri = 4;
DOF = 3 + 3;
y0 = y{1,1,1};
yg = y{1,1,end};
DMP = riemanDMP(M, DOF, N, alpha_z,alpha_x,tau,dt,y0,yg);

%% DMP estimate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DMP = learn(DMP, y, dy, ddy, t);

%% Initial states
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
S.y = DMP.y;
S.g = DMP.g;
S.gnew = DMP.g;

duration = 0;
Q = zeros(round((DMP.tau-duration)/DMP.dt)+1, 12);
W = zeros(round((DMP.tau-duration)/DMP.dt), 6);
dW = zeros(round((DMP.tau-duration)/DMP.dt), 6);
Q(1,1:9) = reshape(S.y.R, 1, 9);
Q(1,10:12) = S.y.t;
S.z = zeros(6,1);
S.zd = zeros(6,1);
W(1,:) = S.z';
dW(1,:) = [0, 0, 0, 0, 0, 0];
S.x = 1;
x1(1) = S.x;
t1(1) = 0;

PSI = [];

% Run DMP
i = 1;
currently_stopping = 1;
while S.x > exp(-DMP.alpha_x * (DMP.tau - duration + DMP.dt) / DMP.tau)
    i = i+1;
    S = integration(DMP, S);
    Q(i,1:9) = reshape(S.y.R, 1, 9);
    Q(i,10:12) = S.y.t;
    W(i,:) = S.z'/DMP.tau;             % angular velocities
    dW(i,:) = S.zd'/DMP.tau;            % angular acc
    t1(i) = t1(i-1) + DMP.dt;
    x1(i) = S.x;
    PSI = [PSI; S.psi];
end

myColors;
rot_fig=figure;hold on
% plot rotation matrix profile generated by DMP
plot(t1, Q(:,1:9),'linewidth',1.5);
plot(t1(1:i), Qorig,'--','linewidth',1.5);
ylabel('$\mathbf{R}$ elements','interpreter','latex','FontName','Times','Fontsize',12)
xlabel('Time [s]','interpreter','latex','FontName','Times','Fontsize',12);
rot_fig.CurrentAxes.TickLabelInterpreter = 'latex';
axis([0,DMP.tau-duration,-1.1,1.1]);

pos_fig=figure;hold on
% plot rotation matrix profile generated by DMP
plot(t1, Q(:,10:12),'linewidth',1.5);
plot(t1, v','--','linewidth',1.5);
ylabel('$\mathbf{R}$ elements','interpreter','latex','FontName','Times','Fontsize',12)
xlabel('Time [s]','interpreter','latex','FontName','Times','Fontsize',12);
pos_fig.CurrentAxes.TickLabelInterpreter = 'latex';
%axis([0,DMP.tau-duration,-1.1,1.1]);
